import Head from 'next/head';
import styles from '../styles/Home.module.css';
import Header from '../components/header';
import Services from '../components/services';
import Work from '../components/work';
import TagLine from '../components/tagLine';
import Testimonials from '../components/testimonails';
import ContactUs from '../components/contactUs';
import Footer from '../components/footer';
export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Find Tech Solutions | A famous company to build websites or mobile apps for your buisness startups or for personal use like portofolios</title>
        <meta name="description" content="A famous company to build websites or mobile apps for your buisness startups or for personal use like portofolios" />
        <link rel="icon" href="/favicon.ico" />
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;700&display=swap" rel="stylesheet" />
      </Head>
      <Header />
      <Services />
      <Work />
      <TagLine />
      <Testimonials />
      <ContactUs />
      <Footer />
    </div>
  )
}
