const Tick = ()=>{
    return(
        <svg id="icons_Q2" data-name="icons Q2" xmlns="http://www.w3.org/2000/svg" width="27.757" height="27.757" viewBox="0 0 27.757 27.757">
            <path id="Path_377" data-name="Path 377" d="M15.878,2A13.815,13.815,0,1,0,25.71,6.046,13.878,13.878,0,0,0,15.878,2Z" transform="translate(-2 -2)" fill="#18a455"/>
            <path id="Path_378" data-name="Path 378" d="M16.164,26.05l-3.785-3.722a1.325,1.325,0,0,1-.126-1.7,1.2,1.2,0,0,1,1.892-.126l2.9,2.9L25,15.452a1.262,1.262,0,0,1,1.766,1.766L17.93,26.05a1.2,1.2,0,0,1-1.766,0Z" transform="translate(-5.692 -6.873)" fill="#fff"/>
        </svg>
    )
}
export default Tick;