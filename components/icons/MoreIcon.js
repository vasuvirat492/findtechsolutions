const MoreIcon = () => {
    return (
        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14">
            <g id="Group_976" data-name="Group 976" transform="translate(1064 -1282)">
                <g id="Ellipse_34" data-name="Ellipse 34" transform="translate(-1064 1282)" fill="rgba(255,255,255,0)" strokeWidth="1">
                    <circle cx="7" cy="7" r="7" stroke="none" />
                    <circle cx="7" cy="7" r="6.5" fill="none" />
                </g>
                <g id="Group_975" data-name="Group 975" transform="translate(-1)">
                    <g id="Ellipse_35" data-name="Ellipse 35" transform="translate(-1060 1288)" fill="rgba(255,255,255,0)" strokeWidth="1">
                        <circle cx="1" cy="1" r="1" stroke="none" />
                        <circle cx="1" cy="1" r="0.5" fill="none" />
                    </g>
                    <g id="Ellipse_36" data-name="Ellipse 36" transform="translate(-1057 1288)" fill="rgba(255,255,255,0)" strokeWidth="1">
                        <circle cx="1" cy="1" r="1" stroke="none" />
                        <circle cx="1" cy="1" r="0.5" fill="none" />
                    </g>
                    <g id="Ellipse_37" data-name="Ellipse 37" transform="translate(-1054 1288)" fill="rgba(255,255,255,0)" strokeWidth="1">
                        <circle cx="1" cy="1" r="1" stroke="none" />
                        <circle cx="1" cy="1" r="0.5" fill="none" />
                    </g>
                </g>
            </g>
        </svg>
    );
}

export default MoreIcon;