import { useEffect } from 'react';
import { gsap } from "gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import styles from './styles/work.module.css';
import ArrowIcon from './icons/Arrow';
import NewTabIcon from './icons/NewTab';
import MoreIcon from './icons/MoreIcon';
gsap.registerPlugin(ScrollTrigger);
const Work = () => {
    useEffect(() => {
        const t1 = gsap.timeline({
            scrollTrigger: {
                trigger: '.work-box',
                start: 'top bottom',
                stagger: 0.3,
            }
        })
        t1.from('.work-box', {
            duration: 0.5,
            opacity: 0,
            y: 100,
            stagger: 0.5,
        })
    }, [])
    return (
        <div className={styles.work}>
            <div className="container">
                <h2 style={{ 'color': '#263238' }}>Our work</h2>
                <div className={styles.workBoxContainer}>
                    <div className={styles.workBox + " work-box"}>
                        <img src="./images/newspaperautomation.jpg" alt="news paper automation" className={styles.image} />
                        <div className={styles.content}>
                            <p className={styles.title}>News Wala</p>
                            <div className={styles.contentTheory}>
                                <div className={styles.withIcon}>
                                    <ArrowIcon />
                                    <p>Done with Django + React JS</p>
                                </div>
                                <div className={styles.withIcon}>
                                    <ArrowIcon />
                                    <p>Online solution for newspaper system</p>
                                </div>
                                <div className={styles.withIcon}>
                                    <ArrowIcon />
                                    <p>Fully functional and responsive Website</p>
                                </div>
                            </div>
                            <div className={styles.exportBtn}>
                                <p>Explore</p>
                                <NewTabIcon />
                            </div>
                        </div>
                    </div>
                    <div className={styles.workBox + " work-box "}>
                        <img src="./images/newspaperautomation.jpg" alt="news paper automation" className={styles.image} />
                        <div className={styles.content}>
                            <p className={styles.title}>News Wala</p>
                            <div className={styles.contentTheory}>
                                <div className={styles.withIcon}>
                                    <ArrowIcon />
                                    <p>Done with Django + React JS</p>
                                </div>
                                <div className={styles.withIcon}>
                                    <ArrowIcon />
                                    <p>Online solution for newspaper system</p>
                                </div>
                                <div className={styles.withIcon}>
                                    <ArrowIcon />
                                    <p>Fully functional and responsive Website</p>
                                </div>
                            </div>
                            <div className={styles.exportBtn}>
                                <p>Explore</p>
                                <NewTabIcon />
                            </div>
                        </div>
                    </div>
                    <div className={styles.exportBtn + " " + styles.moreBtn}>
                        <p>More</p>
                        <MoreIcon />
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Work;