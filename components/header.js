import { useEffect } from 'react';
import { gsap } from "gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import styles from './styles/header.module.css';
import HeroImage from './HeroImage';
gsap.registerPlugin(ScrollTrigger);
const Header = () => {
    useEffect(() => {
        const t1 = gsap.timeline({
            scrollTrigger: {
                trigger: '.hero-image',
                start: 'top bottom'
            }, defaults: { ease: "back.out(2)" }
        })
        const t2 = gsap.timeline({ delay: 0.5 })
        t2.from('#heading1', {
            y: 30,
            opacity: 0,
            duration: 1
        }).from('#heading2', {
            y: 30,
            opacity: 0,
            duration: 0.7
        }).from('.contact-us-btn', {
            y: 30,
            opacity: 0,
            stagger: 0.3,
            duration: 0.6
        })

        t1.from("#svg-image", {
            delay: 2.9,
            opacity: 0,
            y: 100,
            duration: 1,
        }).from("#message1", {
            // delay: 1.3,
            opacity: 0,
            duration: 0.5,
            scale: 0,
            transformOrigin: 'left'
        }).from("#message2", {
            opacity: 0,
            duration: 0.5,
            scale: 0,
            transformOrigin: 'right'
        }).from('#webdemo', {
            opacity: 0,
            transformOrigin: 'top',
            scale: 0,
            y: 5,
            duration: 0.5,
        }).from(".web-lines", {
            opacity: 0,
            duration: 0.5,
            scale: 0,
            stagger: 0.1,
            transformOrigin: 'left'
        }).from("#message3", {
            opacity: 0,
            duration: 0.2,
            scale: 0,
            transformOrigin: 'right'
        }).from(".dot", {
            scale: 0.1,
            stagger: {
                each: 0.1
            },
            duration: 0.5
        })
        // .from(".contact-us-btn",{
        //     opacity:0,
        //     scale:0,
        //     stagger:0.2,
        //     duration:0.4
        // })
        // .from('.navlink',{
        //     y:-100,
        //     opacity:0,
        //     delay:0.3,
        //     stagger:0.1,
        //     duration:0.4,
        // }).from("#brand",{
        //     y:-100,
        //     opacity:0,
        //     delay:0.2,
        //     duration:0.2,
        // })
    }, [])
    return (
        <div className={styles.totalHeader}>
            <nav className={styles.navbar}>
                <span className={styles.brand} id="brand">Find Tech Solutions</span>
                <div className={styles.navlinks}>
                    {/* <div className={styles.navlink + " navlink"}>services</div>
                    <div className={styles.navlink + " navlink"}>work</div>
                    <div className={styles.navlink + " navlink"}>testimonials</div> */}
                    <span>+91 6302957061</span>
                    <div className={styles.navlink + " btn " + styles.contactusbtn + " navlink"}>Contact Us</div>
                </div>
            </nav>
            <div className={styles.herosectioncontainer}>
                <div className={styles.herosection}>
                    <div className={styles.herotext} id="totaltext">
                        <div className={styles.herotextsubheading} id="heading1">HEY YOU</div>
                        <div className={styles.herotextheading} id="heading2">Do you need <span style={{ 'color': '#E8505B' }}>fast and beautiful </span>websites or apps ??</div>
                        <div className={styles.herotextgroup}>
                            <div className={" contact-us-btn btn " + styles.contactusbtnv2}>Contact Us</div>
                            <div style={{ 'color': '#263238', 'marginLeft': '20px', 'fontSize': '1.1rem' }} className={"contact-us-btn align-with-icon"}><img src="./icons/video.svg" alt="video" /><span>Watch Video</span></div>
                        </div>
                    </div>
                    <HeroImage className="hero-image" />
                </div>
            </div>
        </div>
    )
}
export default Header;