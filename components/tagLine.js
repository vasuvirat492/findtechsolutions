import styles from './styles/tagLine.module.css'
const TagLine = ()=>{
    return (
        <div className={styles.tagline}>
            <div className="container">
                <div className={styles.taglineContainer}>
                    <p>Our primary aim is our customer's growth </p>
                    <button>Contact us</button>
                </div>
            </div>
        </div>
    )
}
export default TagLine;