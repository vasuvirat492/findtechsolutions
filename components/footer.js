import styles from './styles/footer.module.css';
const Footer  = ()=>{
    return (
        <div className="container">
            <div className={styles.footerContainer}>
                <div className={styles.footerItemContainer}>
                    <p className={styles.ItemTitle}>Technologies</p>
                    <p>Python</p>
                    <p>Java Script</p>
                    <p>React</p>
                    <p>Django</p>
                    <p>Java</p>
                    <p>Flutter</p>
                </div>
                <div className={styles.footerItemContainer}>
                    <p className={styles.ItemTitle}>Services</p>
                    <p>Websites</p>
                    <p>Android Apps</p>
                    <p>UI/UX design</p>
                    <p>PWA's</p>
                    <p>Cross Platform Apps</p>
                    <p>Desktop Apps</p>
                </div>
                <div className={styles.footerItemContainer}>
                    <p className={styles.ItemTitle}>work flow</p>
                    <p>Requirements</p>
                    <p>Planning</p>
                    <p>wireframing</p>
                    <p>designing</p>
                    <p>coding</p>
                    <p>deploying</p>
                </div>
                <div className={styles.footerItemContainer}>
                    <p className={styles.ItemTitle}>Social Media</p>
                    <div className={styles.iconContainer}>
                        <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 28.571 20">
                            <path id="Path_381" data-name="Path 381" d="M29.987,11.117a3.571,3.571,0,0,0-2.532-2.532C25.247,8,16.286,8,16.286,8S7.325,8,5.117,8.584a3.571,3.571,0,0,0-2.532,2.532A37.9,37.9,0,0,0,2,18a37.9,37.9,0,0,0,.584,6.883,3.571,3.571,0,0,0,2.532,2.532C7.325,28,16.286,28,16.286,28s8.961,0,11.169-.584a3.571,3.571,0,0,0,2.532-2.532A37.9,37.9,0,0,0,30.571,18,37.9,37.9,0,0,0,29.987,11.117ZM13.429,22.286V13.714L20.831,18Z" transform="translate(-2 -8)" />
                        </svg>
                        <p>Youtube</p>
                    </div>
                    <div className={styles.iconContainer}>
                        <svg id="icons_Q2" data-name="icons Q2" xmlns="http://www.w3.org/2000/svg" width="20.22" height="20.169" viewBox="0 0 20.22 20.169">
                            <path id="Path_366" data-name="Path 366" d="M14.11,5.82H18.2a5.459,5.459,0,0,1,1.87.354A3.387,3.387,0,0,1,22,8.094a5.459,5.459,0,0,1,.354,1.87c.051,1.062.051,1.415.051,4.094s0,3.033-.051,4.094A5.459,5.459,0,0,1,22,20.024a3.387,3.387,0,0,1-1.921,1.921,5.459,5.459,0,0,1-1.87.354H10.015a5.459,5.459,0,0,1-1.87-.354,3.387,3.387,0,0,1-1.921-1.921,5.459,5.459,0,0,1-.354-1.87c-.051-1.062-.051-1.415-.051-4.094s0-3.033.051-4.094a5.459,5.459,0,0,1,.354-1.87A3.387,3.387,0,0,1,8.145,6.174a5.459,5.459,0,0,1,1.87-.354H14.11m0-1.82H9.965a9.048,9.048,0,0,0-2.477.455A5.055,5.055,0,0,0,5.719,5.719,4.3,4.3,0,0,0,4.556,7.488a7.33,7.33,0,0,0-.505,2.477C4,11.026,4,11.38,4,14.11s0,3.083.051,4.145a7.33,7.33,0,0,0,.505,2.477A4.3,4.3,0,0,0,5.719,22.5a4.3,4.3,0,0,0,1.769,1.163,7.329,7.329,0,0,0,2.477.505h8.29a7.33,7.33,0,0,0,2.477-.505A4.3,4.3,0,0,0,22.5,22.5a5.055,5.055,0,0,0,1.213-1.769,9.048,9.048,0,0,0,.455-2.477c.051-1.062.051-1.415.051-4.145s0-3.083-.051-4.145a9.048,9.048,0,0,0-.455-2.477A5.055,5.055,0,0,0,22.5,5.719a5.055,5.055,0,0,0-1.769-1.213,9.048,9.048,0,0,0-2.477-.455H14.11" transform="translate(-4 -4)"/>
                            <path id="Path_367" data-name="Path 367" d="M18.907,13.7a5.207,5.207,0,1,0,5.207,5.207A5.207,5.207,0,0,0,18.907,13.7m0,8.593a3.387,3.387,0,1,1,3.387-3.387,3.387,3.387,0,0,1-3.387,3.387" transform="translate(-8.797 -8.797)" />
                            <path id="Path_368" data-name="Path 368" d="M34.726,12.113A1.213,1.213,0,1,1,33.513,10.9a1.213,1.213,0,0,1,1.213,1.213" transform="translate(-17.995 -7.412)" />
                        </svg>
                        <p>Instagram</p>
                    </div>
                    <div className={styles.iconContainer}>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24.556" height="20" viewBox="0 0 24.556 20">
                            <path id="Path_369" data-name="Path 369" d="M26.556,8.372a10.213,10.213,0,0,1-2.9.837,5.246,5.246,0,0,0,2.232-2.79A12.948,12.948,0,0,1,22.7,7.646a5.079,5.079,0,0,0-8.762,3.4,4.241,4.241,0,0,0,.167,1.172A14.4,14.4,0,0,1,3.73,6.977,4.744,4.744,0,0,0,3,9.488a4.967,4.967,0,0,0,2.288,4.186A4.52,4.52,0,0,1,3,13.06H3a4.967,4.967,0,0,0,4.018,4.967,4.8,4.8,0,0,1-1.339.223l-.949-.112a5.134,5.134,0,0,0,4.744,3.516,10.157,10.157,0,0,1-6.251,2.121H2a14.008,14.008,0,0,0,7.7,2.288A14.262,14.262,0,0,0,24.044,11.72v-.726a10.213,10.213,0,0,0,2.511-2.623Z" transform="translate(-2 -6.063)" />
                        </svg>
                        <p>Twitter</p>
                    </div>
                    <div className={styles.iconContainer}>
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 18.085 13.975">
                            <path id="Path_375" data-name="Path 375" d="M19.263,7H2.822A.822.822,0,0,0,2,7.822V20.153a.822.822,0,0,0,.822.822H19.263a.822.822,0,0,0,.822-.822V7.822A.822.822,0,0,0,19.263,7Zm-.822,12.331H3.644V10.987l7.193,4.234a.37.37,0,0,0,.411,0l7.193-4.357Zm-7.193-6a.37.37,0,0,1-.411,0L3.644,8.644h14.8Z" transform="translate(-2 -7)" />
                        </svg>
                        <p>Email</p>
                    </div>
                </div>
            </div>
            <div className={styles.freepik}><a href='https://www.freepik.com/vectors/business'>Business vector created by stories - www.freepik.com</a></div>
        </div>
    )
}
export default Footer;