import { useEffect } from 'react';
import { gsap } from "gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import styles from './styles/contactUs.module.css';
import TickIcon from './icons/Tick'
gsap.registerPlugin(ScrollTrigger);
const ContactUs = () => {
    useEffect(() => {
        const t1 = gsap.timeline({
            scrollTrigger: {
                trigger: '.form-element',
                start: 'top bottom',
            }
        })
        t1.from('.form-element', {
            duration: 0.5,
            opacity: 0,
            y: 100,
            stagger: 0.2,
        })
        const t2 = gsap.timeline({
            scrollTrigger: {
                trigger: '.team-member',
                start: 'top bottom',
            }
        })
        t2.from('.team-member', {
            delay: 0.5,
            duration: 0.5,
            opacity: 0,
            y: 100,
            stagger: 0.2,
        })
    }, [])
    return (
        <div className={styles.contactUs}>
            <div className="container">
                <div className={styles.contactUsContainer}>
                    <div className={styles.formContainer}>
                        <p className={styles.formTitle}>Contact Us</p>
                        <form className={styles.form}>
                            <div className={styles.formElement + " form-element"}>
                                <div className={styles.iconContainer}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14.266" height="19.972" viewBox="0 0 14.266 19.972">
                                        <g id="user" transform="translate(-6.75 -2.25)">
                                            <path id="Path_362" data-name="Path 362" d="M15.118,3.677a3.566,3.566,0,1,1-3.566,3.566,3.566,3.566,0,0,1,3.566-3.566m0-1.427a4.993,4.993,0,1,0,4.993,4.993A4.993,4.993,0,0,0,15.118,2.25Z" transform="translate(-1.235 0)" />
                                            <path id="Path_363" data-name="Path 363" d="M21.016,28.81H19.589V25.243a3.566,3.566,0,0,0-3.566-3.566h-4.28a3.566,3.566,0,0,0-3.566,3.566V28.81H6.75V25.243a4.993,4.993,0,0,1,4.993-4.993h4.28a4.993,4.993,0,0,1,4.993,4.993Z" transform="translate(0 -6.587)" />
                                        </g>
                                    </svg>
                                </div>
                                <input type="text" placeholder="Your Name" onFocus={(e) => e.currentTarget.previousElementSibling.classList.add(styles.iconContainerFocus)} onBlur={(e) => e.currentTarget.previousElementSibling.classList.remove(styles.iconContainerFocus)} />
                            </div>
                            <div className={styles.formElement + " form-element"}>
                                <div className={styles.iconContainer}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16.288" height="20" viewBox="0 0 16.288 20">
                                        <path id="phone" d="M17.774,23.375h-.106C5.361,22.506,3.615,9.752,3.37,5.86a2.668,2.668,0,0,1,.444-1.683,1.789,1.789,0,0,1,1.284-.8H8.549a1.283,1.283,0,0,1,1.165.969l.951,2.877a1.8,1.8,0,0,1-.276,1.662L9.056,10.537c.427,2.979,2.322,5.314,4.747,5.846l1.359-1.654a1.086,1.086,0,0,1,1.359-.315l2.361,1.162a1.578,1.578,0,0,1,.771,1.43v4.062a2.127,2.127,0,0,1-1.879,2.308ZM5.249,4.913a.709.709,0,0,0-.626.769v.062C4.911,10.3,6.758,21.067,17.736,21.837a.559.559,0,0,0,.454-.192.847.847,0,0,0,.21-.531V17.006L16.04,15.844l-1.8,2.192-.3-.046c-5.448-.838-6.187-7.531-6.187-7.6l-.038-.369L9.5,7.813l-.94-2.9Z" transform="translate(-3.364 -3.375)" />
                                    </svg>
                                </div>
                                <input type="text" placeholder="Phone Number" onFocus={(e) => e.currentTarget.previousElementSibling.classList.add(styles.iconContainerFocus)} onBlur={(e) => e.currentTarget.previousElementSibling.classList.remove(styles.iconContainerFocus)} />
                            </div>
                            <textarea placeholder="Your Message" className={styles.textarea + " form-element"}>

                            </textarea>
                        </form>
                        <div className={styles.formElement + " " + styles.sendBtnContainer}>
                            <div className={styles.sendBtn}>
                                <p>Send</p>
                                <svg xmlns="http://www.w3.org/2000/svg" width="18.829" height="18.188" viewBox="0 0 18.829 18.188">
                                    <path id="Path_361" data-name="Path 361" d="M19.932,12.09,5.13,5.078,4.779,5A.779.779,0,0,0,4,5.779l3.1,7.012L4,19.8a.779.779,0,0,0,.779.779L5.13,20.5l14.8-7.012a.779.779,0,0,0,0-1.4ZM6.142,18.283l11.608-5.493h0L6.278,7.16l11.473,5.631Z" transform="matrix(0.985, -0.174, 0.174, 0.985, -4.807, -1.387)" />
                                </svg>
                            </div>
                        </div>
                        <div className={styles.formLastLine}>
                            <TickIcon />
                            <p>Fill out that form will get back to you within 24 hours</p>
                        </div>
                    </div>
                    <div className={styles.teamSection}>
                        <div>
                            <div className={styles.teamContainer}>
                                <div className={styles.teamMember + " team-member"}>
                                    <div className={styles.teamMemberPhoto}>
                                    </div>
                                    <p className={styles.name}>G.Vasu</p>
                                    <p className={styles.position}>Co-Founder,Web Developer</p>
                                </div>
                                <div className={styles.teamMember + " team-member " + styles.lastTeamMember}>
                                    <div className={styles.teamMemberPhoto}>
                                    </div>
                                    <p className={styles.name}>G.Vasu</p>
                                    <p className={styles.position}>Co-Founder,Web Developer</p>
                                </div>
                            </div>
                            <div className={styles.socialInfo}>
                                <p className={styles.tagLine}>Believe Us. We can</p>
                                <div className={styles.socialMedia}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 18.085 13.975">
                                        <path id="Path_375" data-name="Path 375" d="M19.263,7H2.822A.822.822,0,0,0,2,7.822V20.153a.822.822,0,0,0,.822.822H19.263a.822.822,0,0,0,.822-.822V7.822A.822.822,0,0,0,19.263,7Zm-.822,12.331H3.644V10.987l7.193,4.234a.37.37,0,0,0,.411,0l7.193-4.357Zm-7.193-6a.37.37,0,0,1-.411,0L3.644,8.644h14.8Z" transform="translate(-2 -7)" />
                                    </svg>
                                    <p>Email</p>
                                </div>
                                {/* <div className={styles.socialMedia}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 28.571 20">
                                        <path id="Path_381" data-name="Path 381" d="M29.987,11.117a3.571,3.571,0,0,0-2.532-2.532C25.247,8,16.286,8,16.286,8S7.325,8,5.117,8.584a3.571,3.571,0,0,0-2.532,2.532A37.9,37.9,0,0,0,2,18a37.9,37.9,0,0,0,.584,6.883,3.571,3.571,0,0,0,2.532,2.532C7.325,28,16.286,28,16.286,28s8.961,0,11.169-.584a3.571,3.571,0,0,0,2.532-2.532A37.9,37.9,0,0,0,30.571,18,37.9,37.9,0,0,0,29.987,11.117ZM13.429,22.286V13.714L20.831,18Z" transform="translate(-2 -8)" />
                                    </svg>
                                    <p>Youtube</p>
                                </div>
                                <div className={styles.socialMedia}>
                                    <svg id="icons_Q2" data-name="icons Q2" xmlns="http://www.w3.org/2000/svg" width="20.22" height="20.169" viewBox="0 0 20.22 20.169">
                                        <path id="Path_366" data-name="Path 366" d="M14.11,5.82H18.2a5.459,5.459,0,0,1,1.87.354A3.387,3.387,0,0,1,22,8.094a5.459,5.459,0,0,1,.354,1.87c.051,1.062.051,1.415.051,4.094s0,3.033-.051,4.094A5.459,5.459,0,0,1,22,20.024a3.387,3.387,0,0,1-1.921,1.921,5.459,5.459,0,0,1-1.87.354H10.015a5.459,5.459,0,0,1-1.87-.354,3.387,3.387,0,0,1-1.921-1.921,5.459,5.459,0,0,1-.354-1.87c-.051-1.062-.051-1.415-.051-4.094s0-3.033.051-4.094a5.459,5.459,0,0,1,.354-1.87A3.387,3.387,0,0,1,8.145,6.174a5.459,5.459,0,0,1,1.87-.354H14.11m0-1.82H9.965a9.048,9.048,0,0,0-2.477.455A5.055,5.055,0,0,0,5.719,5.719,4.3,4.3,0,0,0,4.556,7.488a7.33,7.33,0,0,0-.505,2.477C4,11.026,4,11.38,4,14.11s0,3.083.051,4.145a7.33,7.33,0,0,0,.505,2.477A4.3,4.3,0,0,0,5.719,22.5a4.3,4.3,0,0,0,1.769,1.163,7.329,7.329,0,0,0,2.477.505h8.29a7.33,7.33,0,0,0,2.477-.505A4.3,4.3,0,0,0,22.5,22.5a5.055,5.055,0,0,0,1.213-1.769,9.048,9.048,0,0,0,.455-2.477c.051-1.062.051-1.415.051-4.145s0-3.083-.051-4.145a9.048,9.048,0,0,0-.455-2.477A5.055,5.055,0,0,0,22.5,5.719a5.055,5.055,0,0,0-1.769-1.213,9.048,9.048,0,0,0-2.477-.455H14.11" transform="translate(-4 -4)"/>
                                        <path id="Path_367" data-name="Path 367" d="M18.907,13.7a5.207,5.207,0,1,0,5.207,5.207A5.207,5.207,0,0,0,18.907,13.7m0,8.593a3.387,3.387,0,1,1,3.387-3.387,3.387,3.387,0,0,1-3.387,3.387" transform="translate(-8.797 -8.797)" />
                                        <path id="Path_368" data-name="Path 368" d="M34.726,12.113A1.213,1.213,0,1,1,33.513,10.9a1.213,1.213,0,0,1,1.213,1.213" transform="translate(-17.995 -7.412)" />
                                    </svg>
                                    <p>Instagram</p>
                                </div>
                                <div className={styles.socialMedia}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24.556" height="20" viewBox="0 0 24.556 20">
                                        <path id="Path_369" data-name="Path 369" d="M26.556,8.372a10.213,10.213,0,0,1-2.9.837,5.246,5.246,0,0,0,2.232-2.79A12.948,12.948,0,0,1,22.7,7.646a5.079,5.079,0,0,0-8.762,3.4,4.241,4.241,0,0,0,.167,1.172A14.4,14.4,0,0,1,3.73,6.977,4.744,4.744,0,0,0,3,9.488a4.967,4.967,0,0,0,2.288,4.186A4.52,4.52,0,0,1,3,13.06H3a4.967,4.967,0,0,0,4.018,4.967,4.8,4.8,0,0,1-1.339.223l-.949-.112a5.134,5.134,0,0,0,4.744,3.516,10.157,10.157,0,0,1-6.251,2.121H2a14.008,14.008,0,0,0,7.7,2.288A14.262,14.262,0,0,0,24.044,11.72v-.726a10.213,10.213,0,0,0,2.511-2.623Z" transform="translate(-2 -6.063)" />
                                    </svg>
                                    <p>Twitter</p>
                                </div> */}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default ContactUs;