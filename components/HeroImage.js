import styles from './styles/header.module.css';
const HeroImage = () => {
  // useEffect(()=>{
  // gsap.from(document.getElementById('svg-image'),{
  //     opacity:0,
  //     scale:0.5,
  //     x:100,
  //     y:100,
  //     duration:2
  // })
  // gsap.from(document.getElementById('message1'),{
  //   y:10,
  //   opacity:0,
  //   scale:0.5,
  //   duration:1
  // })
  // gsap.from(".dot",{
  //   scale:0.1,
  //   duration:1,
  //   stagger:{
  //     each:0.2,
  //     repeat: -1,
  //     yoyo:true,
  //   }
  // })
  // gsap.from('#webdemo',{
  //   y:5,
  //   repeat:-1,
  //   yoyo:true,
  //   duration:1,
  // })
  // },[])
  return (
    <svg id="svg-image" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" viewBox="0 0 461.577 339.197" className={styles.svgImage}>
      <defs>
        <filter id="Rectangle_6" x="221.311" y="0" width="152.365" height="92.162" filterUnits="userSpaceOnUse">
          <feOffset input="SourceAlpha" />
          <feGaussianBlur stdDeviation="2.5" result="blur" />
          <feFlood floodOpacity="0.388" />
          <feComposite operator="in" in2="blur" />
          <feComposite in="SourceGraphic" />
        </filter>
        <filter id="Path_225" x="295.576" y="83.766" width="166.001" height="58.611" filterUnits="userSpaceOnUse">
          <feOffset input="SourceAlpha" />
          <feGaussianBlur stdDeviation="2.5" result="blur-2" />
          <feFlood floodOpacity="0.388" />
          <feComposite operator="in" in2="blur-2" />
          <feComposite in="SourceGraphic" />
        </filter>
        <filter id="Path_275" x="0" y="35.757" width="161.692" height="57.397" filterUnits="userSpaceOnUse">
          <feOffset input="SourceAlpha" />
          <feGaussianBlur stdDeviation="2.5" result="blur-3" />
          <feFlood floodOpacity="0.388" />
          <feComposite operator="in" in2="blur-3" />
          <feComposite in="SourceGraphic" />
        </filter>
        <filter id="Rectangle_109" x="50.028" y="85.785" width="93.858" height="41.286" filterUnits="userSpaceOnUse">
          <feOffset input="SourceAlpha" />
          <feGaussianBlur stdDeviation="2.5" result="blur-4" />
          <feFlood floodOpacity="0.388" />
          <feComposite operator="in" in2="blur-4" />
          <feComposite in="SourceGraphic" />
        </filter>
      </defs>
      <g id="final" transform="translate(-662.43 -126.33)">
        <g id="total" transform="translate(663.961 133.83)">
          <g id="final-2" data-name="final" transform="translate(0 42.569)">
            <g id="Floor" transform="translate(0 280.977)">
              <g id="Group_295" data-name="Group 295">
                <g id="Group_294" data-name="Group 294">
                  <path id="Path_55" data-name="Path 55" d="M385.639,464.99c0,.119-82.7.22-184.7.22-102.027,0-184.712-.1-184.712-.22s82.685-.22,184.712-.22C302.938,464.77,385.639,464.872,385.639,464.99Z" transform="translate(-16.23 -464.77)" fill="#263238" />
                </g>
              </g>
            </g>
            <g id="stand" transform="translate(122.349)">
              <g id="Group_571" data-name="Group 571">
                <g id="Group_316" data-name="Group 316" transform="translate(95.972 29.128)">
                  <g id="Group_300" data-name="Group 300">
                    <g id="Group_299" data-name="Group 299">
                      <g id="Group_298" data-name="Group 298">
                        <g id="Group_297" data-name="Group 297">
                          <g id="Group_296" data-name="Group 296">
                            <path id="Path_56" data-name="Path 56" d="M517.609,180.512h0c1.789-2.595,2.781-4.536,4.045-6.173,1.56-2.018,2.06-2.959,1.67-3.222-.526-.356-.305-.246-3.663,3.019,0,0,4.748-4.7,2.925-4.486-.806.093-4.8,3.951-4.8,3.951,3.485-2.807,4.358-4.418,3.909-4.926-.653-.729-5.461,3.994-5.461,3.994a49.822,49.822,0,0,0,4.2-4.35c.458-.822-.661-.619-1.543-.187-.984.483-6.317,4.121-7.911,5.088a1.1,1.1,0,0,1-1.637-.331,6.923,6.923,0,0,0-3.807-4.121.824.824,0,0,0-.585,1.458,9.844,9.844,0,0,1,1.425,2.34,6.326,6.326,0,0,1,.611,2.094c.093.848.136,1.569.161,2.179l-2.417,13.482,8.5,5.978Z" transform="translate(-504.665 -167.755)" fill="#b78876" />
                          </g>
                        </g>
                      </g>
                    </g>
                  </g>
                  <g id="Group_305" data-name="Group 305" transform="translate(12.998 3.807)">
                    <g id="Group_304" data-name="Group 304">
                      <g id="Group_303" data-name="Group 303">
                        <g id="Group_302" data-name="Group 302">
                          <g id="Group_301" data-name="Group 301">
                            <path id="Path_57" data-name="Path 57" d="M520,176.82a24.145,24.145,0,0,0,4.477-4.57A24.139,24.139,0,0,0,520,176.82Z" transform="translate(-519.995 -172.245)" fill="#aa6550" />
                          </g>
                        </g>
                      </g>
                    </g>
                  </g>
                  <g id="Group_310" data-name="Group 310" transform="translate(11.616 2.636)">
                    <g id="Group_309" data-name="Group 309">
                      <g id="Group_308" data-name="Group 308">
                        <g id="Group_307" data-name="Group 307">
                          <g id="Group_306" data-name="Group 306">
                            <path id="Path_58" data-name="Path 58" d="M518.369,175.456a13.845,13.845,0,0,0,2.68-2.137,13.646,13.646,0,0,0,2.391-2.451,13.847,13.847,0,0,0-2.679,2.137A13.335,13.335,0,0,0,518.369,175.456Z" transform="translate(-518.364 -170.864)" fill="#aa6550" />
                          </g>
                        </g>
                      </g>
                    </g>
                  </g>
                  <g id="Group_315" data-name="Group 315" transform="translate(10.183 1.737)">
                    <g id="Group_314" data-name="Group 314">
                      <g id="Group_313" data-name="Group 313">
                        <g id="Group_312" data-name="Group 312">
                          <g id="Group_311" data-name="Group 311">
                            <path id="Path_59" data-name="Path 59" d="M516.679,174.083a24.634,24.634,0,0,0,4.859-4.274,12.648,12.648,0,0,0-2.569,1.976A11.761,11.761,0,0,0,516.679,174.083Z" transform="translate(-516.674 -169.803)" fill="#aa6550" />
                          </g>
                        </g>
                      </g>
                    </g>
                  </g>
                </g>
                <g id="Group_423" data-name="Group 423" transform="translate(35.124)">
                  <g id="Group_421" data-name="Group 421" transform="translate(0 0.34)">
                    <g id="Group_420" data-name="Group 420">
                      <g id="Group_419" data-name="Group 419">
                        <g id="Group_418" data-name="Group 418">
                          <g id="Group_417" data-name="Group 417">
                            <g id="Group_333" data-name="Group 333" transform="translate(3.891)">
                              <g id="Group_332" data-name="Group 332">
                                <g id="Group_331" data-name="Group 331">
                                  <g id="Group_323" data-name="Group 323">
                                    <g id="Group_322" data-name="Group 322">
                                      <g id="Group_321" data-name="Group 321">
                                        <g id="Group_320" data-name="Group 320">
                                          <g id="Group_319" data-name="Group 319">
                                            <g id="Group_318" data-name="Group 318">
                                              <g id="Group_317" data-name="Group 317">
                                                <path id="Path_60" data-name="Path 60" d="M449.031,133.843a12.5,12.5,0,0,1,13.457,11.71c.348,5.808.67,12.092.627,15.254-.085,6.792-6.877,7.852-6.877,7.852s.975,11.8.873,13.711l-17.976.822-1.628-36.317a12.5,12.5,0,0,1,11.523-13.033Z" transform="translate(-437.495 -133.805)" fill="#b78876" />
                                              </g>
                                            </g>
                                          </g>
                                        </g>
                                      </g>
                                    </g>
                                  </g>
                                  <g id="Group_330" data-name="Group 330" transform="translate(9.891 32.234)">
                                    <g id="Group_329" data-name="Group 329">
                                      <g id="Group_328" data-name="Group 328">
                                        <g id="Group_327" data-name="Group 327">
                                          <g id="Group_326" data-name="Group 326">
                                            <g id="Group_325" data-name="Group 325">
                                              <g id="Group_324" data-name="Group 324">
                                                <path id="Path_61" data-name="Path 61" d="M458.021,174.338a15.83,15.83,0,0,1-8.861-2.518s2.094,4.893,8.869,4.274Z" transform="translate(-449.16 -171.82)" fill="#aa6550" />
                                              </g>
                                            </g>
                                          </g>
                                        </g>
                                      </g>
                                    </g>
                                  </g>
                                </g>
                              </g>
                            </g>
                            <g id="Group_349" data-name="Group 349" transform="translate(0 15.188)">
                              <g id="Group_340" data-name="Group 340">
                                <g id="Group_339" data-name="Group 339">
                                  <g id="Group_338" data-name="Group 338">
                                    <g id="Group_337" data-name="Group 337">
                                      <g id="Group_336" data-name="Group 336">
                                        <g id="Group_335" data-name="Group 335">
                                          <g id="Group_334" data-name="Group 334">
                                            <path id="Path_62" data-name="Path 62" d="M437.429,151.932c-.119-.051-4.672-1.382-4.519,3.265s4.782,3.536,4.782,3.4S437.429,151.932,437.429,151.932Z" transform="translate(-432.906 -151.717)" fill="#b78876" />
                                          </g>
                                        </g>
                                      </g>
                                    </g>
                                  </g>
                                </g>
                              </g>
                              <g id="Group_348" data-name="Group 348" transform="translate(1.184 1.522)">
                                <g id="Group_347" data-name="Group 347">
                                  <g id="Group_346" data-name="Group 346">
                                    <g id="Group_345" data-name="Group 345">
                                      <g id="Group_344" data-name="Group 344">
                                        <g id="Group_343" data-name="Group 343">
                                          <g id="Group_342" data-name="Group 342">
                                            <g id="Group_341" data-name="Group 341">
                                              <path id="Path_63" data-name="Path 63" d="M436.437,157.156c-.017-.017-.076.059-.212.127a.783.783,0,0,1-.6.034,2,2,0,0,1-.984-1.764,2.682,2.682,0,0,1,.187-1.162.939.939,0,0,1,.585-.644.406.406,0,0,1,.483.2c.068.127.042.22.068.229s.093-.085.051-.271a.5.5,0,0,0-.2-.288.608.608,0,0,0-.449-.1,1.13,1.13,0,0,0-.822.746,2.726,2.726,0,0,0-.237,1.306,2.1,2.1,0,0,0,1.246,1.967.877.877,0,0,0,.738-.144C436.429,157.275,436.454,157.165,436.437,157.156Z" transform="translate(-434.302 -153.511)" fill="#aa6550" />
                                            </g>
                                          </g>
                                        </g>
                                      </g>
                                    </g>
                                  </g>
                                </g>
                              </g>
                            </g>
                            <g id="Group_416" data-name="Group 416" transform="translate(12.375 9.494)">
                              <g id="Group_415" data-name="Group 415">
                                <g id="Group_400" data-name="Group 400" transform="translate(0.656 3.654)">
                                  <g id="Group_399" data-name="Group 399">
                                    <g id="Group_398" data-name="Group 398">
                                      <g id="Group_365" data-name="Group 365" transform="translate(10.268)">
                                        <g id="Group_356" data-name="Group 356" transform="translate(1.93 2.035)">
                                          <g id="Group_355" data-name="Group 355">
                                            <g id="Group_354" data-name="Group 354">
                                              <g id="Group_353" data-name="Group 353">
                                                <g id="Group_352" data-name="Group 352">
                                                  <g id="Group_351" data-name="Group 351">
                                                    <g id="Group_350" data-name="Group 350">
                                                      <path id="Path_64" data-name="Path 64" d="M464.593,152.592a.966.966,0,0,1-.933.975.931.931,0,0,1-1-.873.979.979,0,0,1,.933-.984A.943.943,0,0,1,464.593,152.592Z" transform="translate(-462.659 -151.709)" fill="#263238" />
                                                    </g>
                                                  </g>
                                                </g>
                                              </g>
                                            </g>
                                          </g>
                                        </g>
                                        <g id="Group_364" data-name="Group 364">
                                          <g id="Group_363" data-name="Group 363">
                                            <g id="Group_362" data-name="Group 362">
                                              <g id="Group_361" data-name="Group 361">
                                                <g id="Group_360" data-name="Group 360">
                                                  <g id="Group_359" data-name="Group 359">
                                                    <g id="Group_358" data-name="Group 358">
                                                      <g id="Group_357" data-name="Group 357">
                                                        <path id="Path_65" data-name="Path 65" d="M464.222,150.327c-.119.127-.856-.415-1.9-.415s-1.806.526-1.925.4c-.059-.059.068-.288.4-.526a2.667,2.667,0,0,1,1.543-.475,2.51,2.51,0,0,1,1.509.492C464.171,150.048,464.281,150.277,464.222,150.327Z" transform="translate(-460.383 -149.31)" fill="#263238" />
                                                      </g>
                                                    </g>
                                                  </g>
                                                </g>
                                              </g>
                                            </g>
                                          </g>
                                        </g>
                                      </g>
                                      <g id="Group_381" data-name="Group 381" transform="translate(0 0.195)">
                                        <g id="Group_372" data-name="Group 372" transform="translate(1.938 1.839)">
                                          <g id="Group_371" data-name="Group 371">
                                            <g id="Group_370" data-name="Group 370">
                                              <g id="Group_369" data-name="Group 369">
                                                <g id="Group_368" data-name="Group 368">
                                                  <g id="Group_367" data-name="Group 367">
                                                    <g id="Group_366" data-name="Group 366">
                                                      <path id="Path_66" data-name="Path 66" d="M452.493,152.592a.966.966,0,0,1-.933.975.931.931,0,0,1-1-.873.979.979,0,0,1,.933-.984A.948.948,0,0,1,452.493,152.592Z" transform="translate(-450.559 -151.709)" fill="#263238" />
                                                    </g>
                                                  </g>
                                                </g>
                                              </g>
                                            </g>
                                          </g>
                                        </g>
                                        <g id="Group_380" data-name="Group 380">
                                          <g id="Group_379" data-name="Group 379">
                                            <g id="Group_378" data-name="Group 378">
                                              <g id="Group_377" data-name="Group 377">
                                                <g id="Group_376" data-name="Group 376">
                                                  <g id="Group_375" data-name="Group 375">
                                                    <g id="Group_374" data-name="Group 374">
                                                      <g id="Group_373" data-name="Group 373">
                                                        <path id="Path_67" data-name="Path 67" d="M452.112,150.558c-.119.127-.856-.415-1.9-.415s-1.806.526-1.925.4c-.059-.059.068-.288.4-.526a2.659,2.659,0,0,1,1.535-.475,2.51,2.51,0,0,1,1.509.492C452.061,150.278,452.171,150.507,452.112,150.558Z" transform="translate(-448.273 -149.54)" fill="#263238" />
                                                      </g>
                                                    </g>
                                                  </g>
                                                </g>
                                              </g>
                                            </g>
                                          </g>
                                        </g>
                                      </g>
                                      <g id="Group_389" data-name="Group 389" transform="translate(7.134 0.262)">
                                        <g id="Group_388" data-name="Group 388">
                                          <g id="Group_387" data-name="Group 387">
                                            <g id="Group_386" data-name="Group 386">
                                              <g id="Group_385" data-name="Group 385">
                                                <g id="Group_384" data-name="Group 384">
                                                  <g id="Group_383" data-name="Group 383">
                                                    <g id="Group_382" data-name="Group 382">
                                                      <path id="Path_68" data-name="Path 68" d="M457.181,158.311a6.959,6.959,0,0,1,1.7-.305c.263-.025.517-.085.56-.263a1.386,1.386,0,0,0-.178-.789c-.254-.644-.509-1.323-.789-2.027a34.97,34.97,0,0,1-1.772-5.308,34.447,34.447,0,0,1,2.2,5.155c.263.712.517,1.391.755,2.044a1.534,1.534,0,0,1,.136,1.043.649.649,0,0,1-.441.39,1.873,1.873,0,0,1-.449.059A6.554,6.554,0,0,1,457.181,158.311Z" transform="translate(-456.686 -149.619)" fill="#263238" />
                                                    </g>
                                                  </g>
                                                </g>
                                              </g>
                                            </g>
                                          </g>
                                        </g>
                                      </g>
                                      <g id="Group_397" data-name="Group 397" transform="translate(4.395 9.675)">
                                        <g id="Group_396" data-name="Group 396">
                                          <g id="Group_395" data-name="Group 395">
                                            <g id="Group_394" data-name="Group 394">
                                              <g id="Group_393" data-name="Group 393">
                                                <g id="Group_392" data-name="Group 392">
                                                  <g id="Group_391" data-name="Group 391">
                                                    <g id="Group_390" data-name="Group 390">
                                                      <path id="Path_69" data-name="Path 69" d="M453.621,160.72c.17-.008.17,1.119,1.145,1.925s2.179.678,2.188.839c.017.068-.271.22-.78.237a2.822,2.822,0,0,1-1.823-.636,2.424,2.424,0,0,1-.882-1.62C453.417,161,453.544,160.72,453.621,160.72Z" transform="translate(-453.456 -160.72)" fill="#263238" />
                                                    </g>
                                                  </g>
                                                </g>
                                              </g>
                                            </g>
                                          </g>
                                        </g>
                                      </g>
                                    </g>
                                  </g>
                                </g>
                                <g id="Group_407" data-name="Group 407">
                                  <g id="Group_406" data-name="Group 406">
                                    <g id="Group_405" data-name="Group 405">
                                      <g id="Group_404" data-name="Group 404">
                                        <g id="Group_403" data-name="Group 403">
                                          <g id="Group_402" data-name="Group 402">
                                            <g id="Group_401" data-name="Group 401">
                                              <path id="Path_70" data-name="Path 70" d="M452.265,145.78c-.1.28-1.145.153-2.374.3-1.229.119-2.213.483-2.374.229-.076-.119.1-.39.5-.661a4.05,4.05,0,0,1,1.764-.619,3.966,3.966,0,0,1,1.857.22C452.087,145.424,452.308,145.645,452.265,145.78Z" transform="translate(-447.5 -145.001)" fill="#263238" />
                                            </g>
                                          </g>
                                        </g>
                                      </g>
                                    </g>
                                  </g>
                                </g>
                                <g id="Group_414" data-name="Group 414" transform="translate(10.823 0.575)">
                                  <g id="Group_413" data-name="Group 413">
                                    <g id="Group_412" data-name="Group 412">
                                      <g id="Group_411" data-name="Group 411">
                                        <g id="Group_410" data-name="Group 410">
                                          <g id="Group_409" data-name="Group 409">
                                            <g id="Group_408" data-name="Group 408">
                                              <path id="Path_71" data-name="Path 71" d="M463.813,146.757c-.187.237-.9-.008-1.764-.017-.865-.042-1.594.161-1.764-.085-.076-.119.042-.356.365-.585a2.55,2.55,0,0,1,2.841.085C463.8,146.392,463.9,146.638,463.813,146.757Z" transform="translate(-460.264 -145.679)" fill="#263238" />
                                            </g>
                                          </g>
                                        </g>
                                      </g>
                                    </g>
                                  </g>
                                </g>
                              </g>
                            </g>
                          </g>
                        </g>
                      </g>
                    </g>
                  </g>
                  <g id="Group_422" data-name="Group 422" transform="translate(3.92)">
                    <path id="Path_72" data-name="Path 72" d="M437.691,149.028a7.154,7.154,0,0,0,.034,2.128,2.456,2.456,0,0,0,1.187,1.7c1.17.585,2.629-.314,3.154-1.5a7.693,7.693,0,0,0,.254-3.858,18.919,18.919,0,0,1,.076-5.317,7.647,7.647,0,0,1,2.552-4.562,4.837,4.837,0,0,1,5-.721,26.533,26.533,0,0,1,2.646,1.95,4.056,4.056,0,0,0,2.807.7,2.646,2.646,0,0,0,2.154-1.789,2.877,2.877,0,0,0-1.094-2.849,6.574,6.574,0,0,0-2.968-1.187,15.941,15.941,0,0,0-8.819.687,11.482,11.482,0,0,0-6.393,5.952c-1.348,3.18-.441,5.7-.636,9.234" transform="translate(-437.529 -133.403)" fill="#263238" />
                  </g>
                </g>
                <g id="Group_448" data-name="Group 448" transform="translate(21.315 36.908)">
                  <g id="Group_430" data-name="Group 430" transform="translate(0 5.096)">
                    <g id="Group_425" data-name="Group 425" transform="translate(0 0.348)">
                      <g id="Group_424" data-name="Group 424">
                        <path id="Path_73" data-name="Path 73" d="M503.525,186.784,492.417,184.1,485.082,204l-20.266-18.129h0c-2.154-1.577-5.876-1.772-8.7-2.128l-.212-.025c-1.094-.127-1.637-.288-2.3-.365-4.884,3.672-13.245,3.909-18.044.119,0,0-3.638.822-5.808,1.238-6.843,1.314-11.769,6.292-13.134,10.9l8.564,19.757,1.645,18.765-4.74,16.8a2.922,2.922,0,0,0,2.434,3.689l42.431.644.7-.348a7.908,7.908,0,0,0,1.145-5.376c-1.832-8.038-.551-32.018-.941-39.378,8.327,8.089,17.51,14.8,24.217,10.642C497.64,217.344,503.525,186.784,503.525,186.784Z" transform="translate(-416.62 -183.35)" fill="#e8505b" />
                      </g>
                    </g>
                    <g id="Group_429" data-name="Group 429" transform="translate(19.265)">
                      <g id="Group_428" data-name="Group 428">
                        <g id="Group_427" data-name="Group 427">
                          <g id="Group_426" data-name="Group 426">
                            <path id="Path_74" data-name="Path 74" d="M439.34,182.94Z" transform="translate(-439.34 -182.94)" fill="#e8505b" />
                          </g>
                        </g>
                      </g>
                    </g>
                  </g>
                  <g id="Group_432" data-name="Group 432" transform="translate(17.569)">
                    <g id="Group_431" data-name="Group 431">
                      <path id="Path_75" data-name="Path 75" d="M437.34,183.722l.984-6.792,9.251,3.4,2.612,4.6,1.62-5.045,5.071-1.747,2.171,4.511-3.722,5.266-6.978,1.416Z" transform="translate(-437.34 -176.93)" fill="#e8505b" />
                    </g>
                  </g>
                  <g id="Group_435" data-name="Group 435" transform="translate(17.576 3.443)">
                    <g id="Group_434" data-name="Group 434">
                      <g id="Group_433" data-name="Group 433">
                        <path id="Path_76" data-name="Path 76" d="M450.349,186.595c-.017.017-.136-.085-.348-.28a10.107,10.107,0,0,1-.848-.916,13.573,13.573,0,0,1-2.23-3.85l.339.008c-.636,1.594-1.357,3.434-2.137,5.384-.373.924-.738,1.815-1.077,2.663l-.11.28-.212-.212c-1.865-1.806-3.477-3.383-4.613-4.52q-.776-.789-1.323-1.348a2.5,2.5,0,0,1-.441-.517,2.485,2.485,0,0,1,.543.424c.39.348.856.772,1.408,1.263,1.187,1.085,2.832,2.629,4.714,4.418l-.331.068c.339-.848.7-1.738,1.06-2.663.8-1.95,1.535-3.773,2.188-5.367l.178-.441.161.449a14.921,14.921,0,0,0,2.06,3.841C449.95,186.12,450.383,186.561,450.349,186.595Z" transform="translate(-437.349 -180.99)" fill="#263238" />
                      </g>
                    </g>
                  </g>
                  <g id="Group_438" data-name="Group 438" transform="translate(30.877 2.942)">
                    <g id="Group_437" data-name="Group 437">
                      <g id="Group_436" data-name="Group 436">
                        <path id="Path_77" data-name="Path 77" d="M461.434,183.173a7.265,7.265,0,0,1-1.085,1.492,14.786,14.786,0,0,1-3.544,3.112l-.2.127-.085-.229c-.305-.822-.653-1.713-1.026-2.637-.56-1.382-1.077-2.7-1.484-3.867l.356-.025c-.305,1.535-.678,2.781-.907,3.672a8.677,8.677,0,0,1-.407,1.391,5.784,5.784,0,0,1,.187-1.441c.17-.907.492-2.171.763-3.689l.119-.678.237.644c.424,1.153.958,2.451,1.518,3.841.373.924.712,1.823,1.009,2.654l-.288-.1a17,17,0,0,0,3.561-2.934A16.01,16.01,0,0,1,461.434,183.173Z" transform="translate(-453.034 -180.4)" fill="#263238" />
                      </g>
                    </g>
                  </g>
                  <g id="Group_441" data-name="Group 441" transform="translate(30.617 8.691)">
                    <g id="Group_440" data-name="Group 440">
                      <g id="Group_439" data-name="Group 439">
                        <path id="Path_78" data-name="Path 78" d="M456.037,256.677a3.748,3.748,0,0,1,.051-.712c.051-.5.127-1.17.212-2.018.119-.873.178-1.95.271-3.2.042-.627.093-1.3.144-2,.025-.712.059-1.458.093-2.247.144-3.146.136-6.9.059-11.074-.1-4.163-.339-8.742-.661-13.55-.7-9.6-1.637-18.273-2.357-24.539-.356-3.1-.644-5.63-.848-7.428-.093-.848-.161-1.526-.22-2.018q-.064-.7-.051-.712a3.934,3.934,0,0,1,.127.7c.076.492.17,1.17.3,2.01.246,1.747.594,4.282.984,7.411.789,6.266,1.781,14.932,2.476,24.548.331,4.808.543,9.4.627,13.567.059,4.172.034,7.937-.153,11.091-.042.789-.085,1.535-.119,2.247-.059.7-.119,1.374-.178,2-.11,1.246-.2,2.323-.348,3.2-.127.848-.22,1.518-.3,2.01A3.243,3.243,0,0,1,456.037,256.677Z" transform="translate(-452.728 -187.18)" fill="#263238" />
                      </g>
                    </g>
                  </g>
                  <g id="Group_444" data-name="Group 444" transform="translate(68.013 25.718)">
                    <g id="Group_443" data-name="Group 443">
                      <g id="Group_442" data-name="Group 442">
                        <path id="Path_79" data-name="Path 79" d="M497.042,215.315a38.359,38.359,0,0,1,0-8.055,38.37,38.37,0,0,1,0,8.055Z" transform="translate(-496.83 -207.26)" fill="#263238" />
                      </g>
                    </g>
                  </g>
                  <g id="Group_447" data-name="Group 447" transform="translate(68.114 25.734)">
                    <g id="Group_446" data-name="Group 446">
                      <g id="Group_445" data-name="Group 445">
                        <path id="Path_80" data-name="Path 80" d="M499.892,215.827c.034.373-.56-.7-1.806-4.155a18.059,18.059,0,0,1-1.119-4.392,38.943,38.943,0,0,1,1.518,4.257A40.2,40.2,0,0,1,499.892,215.827Z" transform="translate(-496.949 -207.279)" fill="#263238" />
                      </g>
                    </g>
                  </g>
                </g>
                <g id="Group_476" data-name="Group 476" transform="translate(50.026 257.014)">
                  <g id="Group_475" data-name="Group 475">
                    <g id="Group_468" data-name="Group 468">
                      <g id="Group_450" data-name="Group 450">
                        <g id="Group_449" data-name="Group 449">
                          <path id="Path_81" data-name="Path 81" d="M465.438,436.637l-.644,15.407s15.441,5.825,15.6,8.691l-29.915-.178.017-24.047Z" transform="translate(-450.48 -436.51)" fill="#e8505b" />
                        </g>
                      </g>
                      <g id="Group_453" data-name="Group 453" transform="translate(4.191 14.134)">
                        <g id="Group_452" data-name="Group 452">
                          <g id="Group_451" data-name="Group 451">
                            <path id="Path_82" data-name="Path 82" d="M456.3,453.231a1.231,1.231,0,0,0-.856,1.391,1.176,1.176,0,0,0,1.374.856,1.288,1.288,0,0,0,.9-1.475,1.222,1.222,0,0,0-1.526-.738" transform="translate(-455.422 -453.179)" fill="#e0e0e0" />
                          </g>
                        </g>
                      </g>
                      <g id="Group_455" data-name="Group 455" transform="translate(0 21.622)" opacity="0.6">
                        <g id="Group_454" data-name="Group 454">
                          <path id="Path_83" data-name="Path 83" d="M450.48,464.435l.093-2.425,28.643,1.077s1.331.585,1.17,1.526Z" transform="translate(-450.48 -462.01)" fill="#fff" />
                        </g>
                      </g>
                      <g id="Group_458" data-name="Group 458" transform="translate(12.063 15.251)">
                        <g id="Group_457" data-name="Group 457">
                          <g id="Group_456" data-name="Group 456">
                            <path id="Path_84" data-name="Path 84" d="M467.44,454.635c-.008.144-.738.212-1.458.67-.738.449-1.111,1.077-1.238,1.018-.136-.025.085-.9.975-1.45C466.592,454.321,467.474,454.5,467.44,454.635Z" transform="translate(-464.707 -454.496)" fill="#263238" />
                          </g>
                        </g>
                      </g>
                      <g id="Group_461" data-name="Group 461" transform="translate(15.933 16.646)">
                        <g id="Group_460" data-name="Group 460">
                          <g id="Group_459" data-name="Group 459">
                            <path id="Path_85" data-name="Path 85" d="M471.263,456.2c.034.144-.6.39-1.1,1-.517.6-.644,1.263-.789,1.263-.136.025-.237-.831.4-1.586S471.263,456.064,471.263,456.2Z" transform="translate(-469.27 -456.141)" fill="#263238" />
                          </g>
                        </g>
                      </g>
                      <g id="Group_464" data-name="Group 464" transform="translate(19.517 17.952)">
                        <g id="Group_463" data-name="Group 463">
                          <g id="Group_462" data-name="Group 462">
                            <path id="Path_86" data-name="Path 86" d="M473.669,460.2c-.136.034-.331-.7.051-1.5s1.068-1.111,1.128-.992c.085.119-.365.551-.67,1.2S473.813,460.19,473.669,460.2Z" transform="translate(-473.497 -457.681)" fill="#263238" />
                          </g>
                        </g>
                      </g>
                      <g id="Group_467" data-name="Group 467" transform="translate(11.448 11.325)">
                        <g id="Group_466" data-name="Group 466">
                          <g id="Group_465" data-name="Group 465">
                            <path id="Path_87" data-name="Path 87" d="M467.092,450.4c-.059.136-.729-.076-1.56-.025-.831.034-1.475.305-1.543.178-.085-.11.543-.644,1.518-.687S467.16,450.285,467.092,450.4Z" transform="translate(-463.981 -449.865)" fill="#263238" />
                          </g>
                        </g>
                      </g>
                    </g>
                    <g id="Group_471" data-name="Group 471" transform="translate(10.082 6.316)">
                      <g id="Group_470" data-name="Group 470">
                        <g id="Group_469" data-name="Group 469">
                          <path id="Path_88" data-name="Path 88" d="M466.915,446.393a2.87,2.87,0,0,1-1.5-.017,6.983,6.983,0,0,1-1.569-.492,7.694,7.694,0,0,1-.856-.458,1.955,1.955,0,0,1-.449-.356.631.631,0,0,1-.059-.78.788.788,0,0,1,.644-.331,1.807,1.807,0,0,1,.56.085,6.026,6.026,0,0,1,.933.322,5.157,5.157,0,0,1,1.374.916c.687.644.924,1.2.873,1.221-.068.051-.4-.415-1.1-.95a6.053,6.053,0,0,0-1.331-.763,5.063,5.063,0,0,0-.856-.271c-.314-.085-.585-.085-.67.051-.034.051-.034.076.034.17a1.66,1.66,0,0,0,.331.263,7.579,7.579,0,0,0,.789.458,7.905,7.905,0,0,0,1.441.56A10.12,10.12,0,0,1,466.915,446.393Z" transform="translate(-462.371 -443.959)" fill="#263238" />
                        </g>
                      </g>
                    </g>
                    <g id="Group_474" data-name="Group 474" transform="translate(14.045 4.214)">
                      <g id="Group_473" data-name="Group 473">
                        <g id="Group_472" data-name="Group 472">
                          <path id="Path_89" data-name="Path 89" d="M467.439,446.228a2.384,2.384,0,0,1-.39-1.45,4.931,4.931,0,0,1,.212-1.637,4.647,4.647,0,0,1,.39-.907,1.246,1.246,0,0,1,.958-.755.7.7,0,0,1,.627.424,1.865,1.865,0,0,1,.153.551,3.344,3.344,0,0,1-.008,1.009,3.488,3.488,0,0,1-.653,1.543c-.594.772-1.2.941-1.213.89-.051-.076.466-.356.916-1.094a3.448,3.448,0,0,0,.483-1.408,2.976,2.976,0,0,0-.025-.865c-.042-.322-.161-.577-.28-.543-.153,0-.407.237-.526.483a4.965,4.965,0,0,0-.373.814,5.654,5.654,0,0,0-.3,1.492C467.363,445.652,467.515,446.2,467.439,446.228Z" transform="translate(-467.043 -441.48)" fill="#263238" />
                        </g>
                      </g>
                    </g>
                  </g>
                </g>
                <g id="Group_502" data-name="Group 502" transform="translate(0 252.613)">
                  <g id="Group_478" data-name="Group 478">
                    <g id="Group_477" data-name="Group 477">
                      <path id="Path_90" data-name="Path 90" d="M404.861,431.32,400,450.6l-8.242,14.143a2.032,2.032,0,0,0,.856,2.849h0a2.015,2.015,0,0,0,1.865-.034c3.7-1.993,17.272-9.412,17.552-10.837.322-1.654,6.478-20,6.478-20Z" transform="translate(-391.482 -431.32)" fill="#e8505b" />
                    </g>
                  </g>
                  <g id="Group_480" data-name="Group 480" transform="translate(1.134 24.124)" opacity="0.6">
                    <g id="Group_479" data-name="Group 479">
                      <path id="Path_91" data-name="Path 91" d="M392.82,471.929l19.774-12.159-.22.814a3.056,3.056,0,0,1-1.229,1.721c-1.611,1.094-6.131,4.019-16.4,9.616a2.086,2.086,0,0,1-1.925.008Z" transform="translate(-392.82 -459.77)" fill="#fff" />
                    </g>
                  </g>
                  <g id="Group_483" data-name="Group 483" transform="translate(17.886 16.774)" opacity="0.6">
                    <g id="Group_482" data-name="Group 482">
                      <g id="Group_481" data-name="Group 481">
                        <path id="Path_92" data-name="Path 92" d="M413.376,451.177a1.234,1.234,0,0,0-.755,1.458,1.191,1.191,0,0,0,1.441.763,1.3,1.3,0,0,0,.789-1.552,1.226,1.226,0,0,0-1.586-.627" transform="translate(-412.576 -451.103)" fill="#fff" />
                      </g>
                    </g>
                  </g>
                  <g id="Group_486" data-name="Group 486" transform="translate(4.475 26.028)">
                    <g id="Group_485" data-name="Group 485">
                      <g id="Group_484" data-name="Group 484">
                        <path id="Path_93" data-name="Path 93" d="M400.118,465.106a4.415,4.415,0,0,0-3.358-2.993c0-.051.28-.136.746-.076a3.259,3.259,0,0,1,2.6,2.315C400.22,464.809,400.169,465.106,400.118,465.106Z" transform="translate(-396.76 -462.016)" fill="#263238" />
                      </g>
                    </g>
                  </g>
                  <g id="Group_489" data-name="Group 489" transform="translate(2.482 29.155)">
                    <g id="Group_488" data-name="Group 488">
                      <g id="Group_487" data-name="Group 487">
                        <path id="Path_94" data-name="Path 94" d="M397.683,467.854c-.11.059-.56-.738-1.467-1.3a16,16,0,0,0-1.806-.755,2.271,2.271,0,0,1,2.035.39C397.479,466.837,397.793,467.829,397.683,467.854Z" transform="translate(-394.409 -465.704)" fill="#263238" />
                      </g>
                    </g>
                  </g>
                  <g id="Group_492" data-name="Group 492" transform="translate(6.484 22.904)">
                    <g id="Group_491" data-name="Group 491">
                      <g id="Group_490" data-name="Group 490">
                        <path id="Path_95" data-name="Path 95" d="M403.929,461.959a20.862,20.862,0,0,0-2.137-2.145,19.378,19.378,0,0,0-2.662-1.45,5.592,5.592,0,0,1,4.8,3.6Z" transform="translate(-399.13 -458.332)" fill="#263238" />
                      </g>
                    </g>
                  </g>
                  <g id="Group_495" data-name="Group 495" transform="translate(8.316 19.023)">
                    <g id="Group_494" data-name="Group 494">
                      <g id="Group_493" data-name="Group 493">
                        <path id="Path_96" data-name="Path 96" d="M406.225,455.523c-.085.1-1.018-.636-2.383-1.068-1.357-.449-2.544-.4-2.552-.534a3.905,3.905,0,0,1,2.688.119A3.982,3.982,0,0,1,406.225,455.523Z" transform="translate(-401.29 -453.754)" fill="#263238" />
                      </g>
                    </g>
                  </g>
                  <g id="Group_498" data-name="Group 498" transform="translate(9.647 13.438)">
                    <g id="Group_497" data-name="Group 497">
                      <g id="Group_496" data-name="Group 496">
                        <path id="Path_97" data-name="Path 97" d="M408.032,448.9a9.988,9.988,0,0,0-2.451-1.179,10.393,10.393,0,0,0-2.722-.068c-.017-.051.254-.229.772-.365a4.277,4.277,0,0,1,2.052.009,4.454,4.454,0,0,1,1.832.924C407.905,448.577,408.066,448.866,408.032,448.9Z" transform="translate(-402.859 -447.168)" fill="#263238" />
                      </g>
                    </g>
                  </g>
                  <g id="Group_501" data-name="Group 501" transform="translate(10.281 9.173)">
                    <g id="Group_500" data-name="Group 500">
                      <g id="Group_499" data-name="Group 499">
                        <path id="Path_98" data-name="Path 98" d="M406.026,445.9a1.587,1.587,0,0,1,.178-.729,5.13,5.13,0,0,1,1.1-1.738,2.35,2.35,0,0,1,1.255-.806.809.809,0,0,1,.839.424,1.32,1.32,0,0,1,.068.95,2.885,2.885,0,0,1-3.146,1.9,3.338,3.338,0,0,1-2.7-2.561,1.16,1.16,0,0,1,.2-.941.808.808,0,0,1,.924-.187,2.927,2.927,0,0,1,1.085,1.009,5.719,5.719,0,0,1,.9,1.848,1.777,1.777,0,0,1,.11.746,9.281,9.281,0,0,0-1.255-2.408,2.68,2.68,0,0,0-.967-.856.416.416,0,0,0-.5.1.8.8,0,0,0-.1.628,2.969,2.969,0,0,0,2.366,2.188,2.5,2.5,0,0,0,2.688-1.569.97.97,0,0,0-.017-.661A.438.438,0,0,0,408.6,443a2.153,2.153,0,0,0-1.077.661A7.417,7.417,0,0,0,406.026,445.9Z" transform="translate(-403.607 -442.138)" fill="#263238" />
                      </g>
                    </g>
                  </g>
                </g>
                <g id="Group_504" data-name="Group 504" transform="translate(46.261 43.394)">
                  <g id="Group_503" data-name="Group 503">
                    <path id="Path_99" data-name="Path 99" d="M447.609,184.58l2.1,1.094-.39,3.536-3.281-.687Z" transform="translate(-446.04 -184.58)" fill="#525bf4" />
                  </g>
                </g>
                <g id="Group_522" data-name="Group 522" transform="translate(46.261 42.869)">
                  <g id="Group_506" data-name="Group 506" transform="translate(0 0.526)" opacity="0.4">
                    <g id="Group_505" data-name="Group 505">
                      <path id="Path_100" data-name="Path 100" d="M447.609,184.58l2.1,1.094-.39,3.536-3.281-.687Z" transform="translate(-446.04 -184.58)" />
                    </g>
                  </g>
                  <g id="Group_508" data-name="Group 508" transform="translate(6.385)">
                    <g id="Group_507" data-name="Group 507">
                      <path id="Path_101" data-name="Path 101" d="M453.57,185.359l1.586-1.4,1.458,3.705-2.086,1Z" transform="translate(-453.57 -183.96)" fill="#525bf4" />
                    </g>
                  </g>
                  <g id="Group_510" data-name="Group 510" transform="translate(3.078 5.8)">
                    <g id="Group_509" data-name="Group 509">
                      <path id="Path_102" data-name="Path 102" d="M450.654,191.02l-.984,41.812,7.063,6.987,3.426-9.285L453.375,190.8Z" transform="translate(-449.67 -190.8)" fill="#fff" />
                    </g>
                  </g>
                  <g id="Group_512" data-name="Group 512" transform="translate(2.365 0.007)">
                    <g id="Group_511" data-name="Group 511">
                      <path id="Path_103" data-name="Path 103" d="M450.568,184.292c.483-.059,1.145-.187,1.67-.3a1.155,1.155,0,0,1,1.263.585l.89,1.645a1.163,1.163,0,0,1,.119.772l-.356,1.848a1.171,1.171,0,0,1-.907.916l-2.018.407a1.155,1.155,0,0,1-1.263-.611l-1.009-1.993a1.133,1.133,0,0,1-.025-.992l.7-1.594A1.134,1.134,0,0,1,450.568,184.292Z" transform="translate(-448.83 -183.968)" fill="#fff" />
                    </g>
                  </g>
                  <g id="Group_515" data-name="Group 515" transform="translate(3.077 0.771)">
                    <g id="Group_514" data-name="Group 514">
                      <g id="Group_513" data-name="Group 513">
                        <path id="Path_104" data-name="Path 104" d="M453.63,184.87a4.8,4.8,0,0,1-.534.67c-.348.4-.839.941-1.382,1.535s-1.051,1.119-1.416,1.5a5.1,5.1,0,0,1-.627.594,4.1,4.1,0,0,1,.534-.67c.348-.4.839-.941,1.382-1.535s1.051-1.119,1.416-1.5A5.834,5.834,0,0,1,453.63,184.87Z" transform="translate(-449.669 -184.869)" fill="#263238" />
                      </g>
                    </g>
                  </g>
                  <g id="Group_518" data-name="Group 518" transform="translate(3.144 0.931)">
                    <g id="Group_517" data-name="Group 517">
                      <g id="Group_516" data-name="Group 516">
                        <path id="Path_105" data-name="Path 105" d="M451.581,186.823a4.727,4.727,0,0,1-.975-.822,4.562,4.562,0,0,1-.856-.941,4.728,4.728,0,0,1,.975.823A6,6,0,0,1,451.581,186.823Z" transform="translate(-449.747 -185.057)" fill="#263238" />
                      </g>
                    </g>
                  </g>
                  <g id="Group_521" data-name="Group 521" transform="translate(7.076 0.712)">
                    <g id="Group_520" data-name="Group 520">
                      <g id="Group_519" data-name="Group 519">
                        <path id="Path_106" data-name="Path 106" d="M454.644,189.464a22.429,22.429,0,0,1,.5-2.408,2.385,2.385,0,0,0-.509-1.569,1.45,1.45,0,0,1-.246-.687c.025-.008.1.254.348.627a7.125,7.125,0,0,1,.407.678,1.626,1.626,0,0,1,.161.975c-.136.687-.314,1.263-.432,1.7A3.078,3.078,0,0,1,454.644,189.464Z" transform="translate(-454.386 -184.8)" fill="#263238" />
                      </g>
                    </g>
                  </g>
                </g>
                <g id="Group_545" data-name="Group 545" transform="translate(4.843 113.518)">
                  <g id="Group_530" data-name="Group 530">
                    <g id="Group_529" data-name="Group 529">
                      <g id="Group_528" data-name="Group 528">
                        <g id="Group_527" data-name="Group 527">
                          <g id="Group_526" data-name="Group 526">
                            <g id="Group_525" data-name="Group 525">
                              <g id="Group_524" data-name="Group 524">
                                <g id="Group_523" data-name="Group 523">
                                  <path id="Path_107" data-name="Path 107" d="M464,267.924l-2.968,153.493a1.88,1.88,0,0,1-1.9,1.849l-17.883-.161a1.879,1.879,0,0,1-1.865-1.925l2.773-132.116L418.9,420.2a3.379,3.379,0,0,1-3.443,2.781l-15-.517a3.377,3.377,0,0,1-3.222-3.892L420.78,267.28Z" transform="translate(-397.193 -267.28)" fill="#263238" />
                                </g>
                              </g>
                            </g>
                          </g>
                        </g>
                      </g>
                    </g>
                  </g>
                  <g id="Group_533" data-name="Group 533" transform="translate(10.537 8.53)">
                    <g id="Group_532" data-name="Group 532">
                      <g id="Group_531" data-name="Group 531">
                        <path id="Path_108" data-name="Path 108" d="M409.62,416.4s.008-.127.042-.365c.042-.254.093-.611.161-1.051.153-.95.373-2.306.661-4.053.594-3.536,1.441-8.624,2.484-14.9,2.094-12.592,4.994-29.958,8.183-49.138,3.171-19.2,5.978-36.588,7.962-49.18.984-6.283,1.772-11.379,2.323-14.915.271-1.747.492-3.112.636-4.053.076-.449.136-.8.178-1.051.042-.237.076-.356.076-.356a2.885,2.885,0,0,1-.034.365c-.034.254-.076.611-.136,1.06-.127.95-.322,2.315-.56,4.07-.492,3.536-1.246,8.632-2.2,14.941-1.908,12.609-4.672,30.008-7.843,49.206s-6.122,36.563-8.31,49.121c-1.1,6.266-1.993,11.337-2.612,14.873-.314,1.738-.568,3.095-.738,4.036-.085.449-.153.8-.2,1.051C409.654,416.282,409.62,416.4,409.62,416.4Z" transform="translate(-409.62 -277.34)" fill="#455a64" />
                      </g>
                    </g>
                  </g>
                  <g id="Group_536" data-name="Group 536" transform="translate(56.334 8.632)">
                    <g id="Group_535" data-name="Group 535">
                      <g id="Group_534" data-name="Group 534">
                        <path id="Path_109" data-name="Path 109" d="M463.664,416.979c-.119,0,.085-31.238.449-69.759.365-38.539.755-69.759.865-69.759s-.085,31.229-.449,69.768C464.164,385.75,463.774,416.979,463.664,416.979Z" transform="translate(-463.63 -277.46)" fill="#455a64" />
                      </g>
                    </g>
                  </g>
                  <g id="Group_539" data-name="Group 539" transform="translate(42.606 148.473)">
                    <g id="Group_538" data-name="Group 538">
                      <g id="Group_537" data-name="Group 537">
                        <path id="Path_110" data-name="Path 110" d="M469.215,442.592c0,.119-4.876.212-10.887.212s-10.887-.093-10.887-.212,4.876-.212,10.887-.212S469.215,442.482,469.215,442.592Z" transform="translate(-447.44 -442.38)" fill="#455a64" />
                      </g>
                    </g>
                  </g>
                  <g id="Group_542" data-name="Group 542" transform="translate(0.548 147.722)">
                    <g id="Group_541" data-name="Group 541">
                      <g id="Group_540" data-name="Group 540">
                        <path id="Path_111" data-name="Path 111" d="M420.064,441.626a3.689,3.689,0,0,1-.873.085c-.653.025-1.45.051-2.383.085-2.111.051-4.842.119-7.852.2-3.07.059-5.851.085-7.86.068-1.009-.009-1.823-.025-2.383-.051a4.076,4.076,0,0,1-.873-.076,3.687,3.687,0,0,1,.873-.085c.653-.025,1.45-.051,2.383-.085,2.111-.051,4.842-.119,7.86-.2,3.07-.059,5.851-.085,7.86-.068,1.009.008,1.823.025,2.383.051A3.937,3.937,0,0,1,420.064,441.626Z" transform="translate(-397.84 -441.494)" fill="#455a64" />
                      </g>
                    </g>
                  </g>
                  <g id="Group_544" data-name="Group 544" transform="translate(44.147 2.459)">
                    <g id="Group_543" data-name="Group 543">
                      <path id="Path_112" data-name="Path 112" d="M453.194,270.18a15.963,15.963,0,0,1-.059,2.654c-.093,1.637-.246,3.892-.449,6.385-.11,1.246-.178,2.434-.331,3.519a6.735,6.735,0,0,1-.95,2.773,3.791,3.791,0,0,1-1.45,1.348c-.424.212-.687.229-.7.2a4.471,4.471,0,0,0,1.882-1.721,6.848,6.848,0,0,0,.814-2.663c.127-1.06.187-2.256.3-3.5.2-2.493.407-4.748.577-6.376A16.171,16.171,0,0,1,453.194,270.18Z" transform="translate(-449.258 -270.18)" fill="#455a64" />
                    </g>
                  </g>
                </g>
                <g id="Group_570" data-name="Group 570" transform="translate(8.076 53.646)">
                  <g id="Group_548" data-name="Group 548" transform="translate(21.574 31.725)" opacity="0.3">
                    <g id="Group_547" data-name="Group 547">
                      <g id="Group_546" data-name="Group 546">
                        <path id="Path_113" data-name="Path 113" d="M426.654,250.064a7.994,7.994,0,0,0,6.97,2c1.755-.356,3.426-1.314,5.215-1.2,1.959.127,3.578,1.5,5.367,2.306,3.341,1.509,7.233,1.026,10.769.085a8.015,8.015,0,0,0,4.375-2.34,7.864,7.864,0,0,0,1.348-3.638,37.32,37.32,0,0,0,.526-7.207,6.923,6.923,0,0,0-1.162-4.3,4.944,4.944,0,0,0-3.231-1.628,12.858,12.858,0,0,0-3.689.2A60.866,60.866,0,0,0,426.45,245.8" transform="translate(-426.45 -234.084)" />
                      </g>
                    </g>
                  </g>
                  <g id="Group_551" data-name="Group 551">
                    <g id="Group_550" data-name="Group 550">
                      <g id="Group_549" data-name="Group 549">
                        <path id="Path_114" data-name="Path 114" d="M439.989,236.4s-22.453,10.04-24.734,10.54c-7.581,1.662-16.246-5.495-13.838-13.66.9-3.036,12.973-36.614,12.973-36.614l11.5,15.89-4.867,16.263,12.736-.848Z" transform="translate(-401.006 -196.67)" fill="#e8505b" />
                      </g>
                    </g>
                  </g>
                  <g id="Group_554" data-name="Group 554" transform="translate(19.743 15.899)">
                    <g id="Group_553" data-name="Group 553">
                      <g id="Group_552" data-name="Group 552">
                        <path id="Path_115" data-name="Path 115" d="M436.263,230.963a4.343,4.343,0,0,1-.89.127c-.644.051-1.467.127-2.459.212-2.086.161-5.011.365-8.344.577l-.28.017.076-.271c.237-.814.492-1.653.746-2.518,1.136-3.8,2.188-7.224,2.968-9.7.382-1.179.7-2.162.941-2.925a6.088,6.088,0,0,1,.4-1.06,5.906,5.906,0,0,1-.246,1.1c-.212.772-.492,1.764-.822,2.968-.712,2.5-1.713,5.944-2.849,9.734-.263.865-.509,1.7-.755,2.51l-.2-.254c3.332-.2,6.258-.348,8.352-.441.992-.034,1.815-.068,2.467-.093A4.641,4.641,0,0,1,436.263,230.963Z" transform="translate(-424.29 -215.42)" fill="#263238" />
                      </g>
                    </g>
                  </g>
                  <g id="Group_557" data-name="Group 557" transform="translate(14.307 41.045)">
                    <g id="Group_556" data-name="Group 556">
                      <g id="Group_555" data-name="Group 555">
                        <path id="Path_116" data-name="Path 116" d="M440.045,245.079c.042.1-4.884,2.23-11.006,4.748s-11.116,4.486-11.159,4.384,4.884-2.23,11.006-4.757S440,244.978,440.045,245.079Z" transform="translate(-417.88 -245.076)" fill="#263238" />
                      </g>
                    </g>
                  </g>
                  <g id="Group_569" data-name="Group 569" transform="translate(31.792 20.74)">
                    <g id="Group_566" data-name="Group 566">
                      <g id="Group_560" data-name="Group 560" transform="translate(0.171)">
                        <g id="Group_559" data-name="Group 559">
                          <g id="Group_558" data-name="Group 558">
                            <path id="Path_117" data-name="Path 117" d="M440.5,231.085l-1.772-8.623a1.111,1.111,0,0,1,1.094-1.331l22.318.1a1.1,1.1,0,0,1,1.094,1.238l-2.306,19.748a1.55,1.55,0,0,1-1.543,1.365l-14.551-.11a1.55,1.55,0,0,1-1.484-1.162Z" transform="translate(-438.702 -221.13)" fill="#455a64" />
                          </g>
                        </g>
                      </g>
                      <g id="Group_565" data-name="Group 565" transform="translate(0 5.418)">
                        <g id="Group_564" data-name="Group 564">
                          <g id="Group_563" data-name="Group 563">
                            <g id="Group_562" data-name="Group 562">
                              <g id="Group_561" data-name="Group 561">
                                <path id="Path_118" data-name="Path 118" d="M438.5,227.52h0a2.342,2.342,0,0,1,2.188,1.5,5.116,5.116,0,0,0,1.492,2.391c1.094.594,2.985,1.077,3.985.585s4.977-4.07,5.681-3.672,1.3,1.3.6,2.094-4.384,3.188-3.689,3.985,8.471-2.713,9.666-2.4,1.891,1.043,1.2,1.526-5.978,2.467-6.275,3.171c0,0,7.971-2.094,8.267-1.594s.8,1.3,0,1.594-7.674,2.094-8.267,2.391a14.517,14.517,0,0,1-3.07.9c-.22,0-1.781.526-3.214,1.018a7.041,7.041,0,0,1-4.96-.136l-.806-.331a3.17,3.17,0,0,1-1.967-3.12l.161-2.713-.873-2.239Z" transform="translate(-438.5 -227.52)" fill="#b78876" />
                              </g>
                            </g>
                          </g>
                        </g>
                      </g>
                    </g>
                    <g id="Group_568" data-name="Group 568" transform="translate(11.769 2.51)">
                      <g id="Group_567" data-name="Group 567">
                        <path id="Path_119" data-name="Path 119" d="M454.008,224.9a.814.814,0,1,1-.814-.814A.814.814,0,0,1,454.008,224.9Z" transform="translate(-452.38 -224.09)" fill="#fafafa" />
                      </g>
                    </g>
                  </g>
                </g>
              </g>
            </g>
            <g id="desk" transform="translate(13.447 108.21)">
              <g id="Group_651" data-name="Group 651" transform="translate(191.277)">
                <g id="Group_572" data-name="Group 572" transform="translate(0 36.8)">
                  <path id="Path_120" data-name="Path 120" d="M555.506,306.9H488.63V304.42h64.384a2.479,2.479,0,0,1,2.493,2.484Z" transform="translate(-488.63 -304.42)" fill="#455a64" />
                </g>
                <g id="Group_573" data-name="Group 573" transform="translate(0)">
                  <path id="Path_121" data-name="Path 121" d="M537.03,299.771H491.462a2.832,2.832,0,0,1-2.832-2.832V263.852a2.832,2.832,0,0,1,2.832-2.832H537.03a2.832,2.832,0,0,1,2.832,2.832v33.086A2.821,2.821,0,0,1,537.03,299.771Z" transform="translate(-488.63 -261.02)" fill="#455a64" />
                </g>
                <g id="Group_650" data-name="Group 650" transform="translate(2.077 1.789)">
                  <g id="Group_577" data-name="Group 577">
                    <g id="Group_574" data-name="Group 574" transform="translate(0.068 0.059)">
                      <rect id="Rectangle_90" data-name="Rectangle 90" width="46.959" height="34.511" fill="#fafafa" />
                    </g>
                    <g id="Group_576" data-name="Group 576">
                      <g id="Group_575" data-name="Group 575">
                        <path id="Path_122" data-name="Path 122" d="M538.1,297.7s-.009-.22-.009-.636,0-1.034-.008-1.84c0-1.611-.008-3.985-.017-7-.008-6.029-.017-14.652-.025-25.031l.059.051c-13.491,0-29.635.008-46.95.008h0l.068-.059c0,12.532-.008,24.336-.008,34.511l-.059-.051c14,.008,25.735.017,33.968.025,4.121.009,7.369.009,9.582.017,1.111,0,1.959.008,2.535.008h.653a1.008,1.008,0,0,1,.212-.008h-.857c-.577,0-1.416,0-2.527.008-2.213,0-5.452.009-9.565.017-8.242.008-19.986.017-34,.025h-.059V297.7c0-10.175-.008-21.978-.008-34.511v-.059h.068c17.315,0,33.459.008,46.95.008h.059v.051c-.008,10.4-.026,19.028-.026,25.065-.008,3.01-.008,5.376-.017,6.987,0,.806-.009,1.416-.009,1.832S538.1,297.7,538.1,297.7Z" transform="translate(-491.08 -263.13)" fill="#263238" />
                      </g>
                    </g>
                  </g>
                  <g id="Group_579" data-name="Group 579" transform="translate(7.504 9.285)">
                    <g id="Group_578" data-name="Group 578">
                      <path id="Path_123" data-name="Path 123" d="M532.423,274.139s-.051,0-.161.008-.271,0-.475.008c-.424,0-1.034.008-1.831.008-1.6,0-3.917.008-6.817.017-5.8.009-13.906.009-23.149.017l.059-.059v15.933l-.059-.059c9.183.008,17.238.017,22.987.017,2.875.008,5.172.017,6.758.017.789,0,1.4.008,1.815.008.2,0,.365,0,.466.008a1.26,1.26,0,0,1,.161.008s-.051,0-.161.008-.263,0-.466.009c-.416,0-1.026.008-1.815.008-1.586,0-3.884.008-6.758.017-5.757.008-13.8.008-22.987.017h-.059V274.08h.059c9.242.008,17.349.017,23.149.017,2.9.008,5.215.017,6.817.017.8.008,1.408.008,1.831.008.2,0,.365,0,.475.008S532.423,274.139,532.423,274.139Z" transform="translate(-499.93 -274.08)" fill="#e0e0e0" />
                    </g>
                  </g>
                  <g id="Group_581" data-name="Group 581" transform="translate(7.572 11.964)">
                    <g id="Group_580" data-name="Group 580">
                      <path id="Path_124" data-name="Path 124" d="M532.435,277.3c0,.034-7.258.059-16.213.059s-16.212-.025-16.212-.059,7.258-.059,16.212-.059S532.435,277.274,532.435,277.3Z" transform="translate(-500.01 -277.24)" fill="#e0e0e0" />
                    </g>
                  </g>
                  <g id="Group_583" data-name="Group 583" transform="translate(7.335 14.101)">
                    <g id="Group_582" data-name="Group 582">
                      <path id="Path_125" data-name="Path 125" d="M532.155,279.819c0,.034-7.258.059-16.213.059s-16.212-.025-16.212-.059,7.258-.059,16.212-.059S532.155,279.794,532.155,279.819Z" transform="translate(-499.73 -279.76)" fill="#e0e0e0" />
                    </g>
                  </g>
                  <g id="Group_585" data-name="Group 585" transform="translate(7.572 16.357)">
                    <g id="Group_584" data-name="Group 584">
                      <path id="Path_126" data-name="Path 126" d="M532.435,282.479c0,.034-7.258.059-16.213.059s-16.212-.025-16.212-.059,7.258-.059,16.212-.059S532.435,282.445,532.435,282.479Z" transform="translate(-500.01 -282.42)" fill="#e0e0e0" />
                    </g>
                  </g>
                  <g id="Group_587" data-name="Group 587" transform="translate(7.572 18.553)">
                    <g id="Group_586" data-name="Group 586">
                      <path id="Path_127" data-name="Path 127" d="M532.435,285.069c0,.034-7.258.059-16.213.059s-16.212-.025-16.212-.059,7.258-.059,16.212-.059S532.435,285.035,532.435,285.069Z" transform="translate(-500.01 -285.01)" fill="#e0e0e0" />
                    </g>
                  </g>
                  <g id="Group_589" data-name="Group 589" transform="translate(7.572 20.749)">
                    <g id="Group_588" data-name="Group 588">
                      <path id="Path_128" data-name="Path 128" d="M532.435,287.659c0,.034-7.258.059-16.213.059s-16.212-.025-16.212-.059,7.258-.059,16.212-.059S532.435,287.625,532.435,287.659Z" transform="translate(-500.01 -287.6)" fill="#e0e0e0" />
                    </g>
                  </g>
                  <g id="Group_591" data-name="Group 591" transform="translate(7.564 22.945)">
                    <g id="Group_590" data-name="Group 590">
                      <path id="Path_129" data-name="Path 129" d="M500,290.249c0-.034,7.258-.059,16.212-.059s16.213.025,16.213.059-7.258.059-16.213.059S500,290.275,500,290.249Z" transform="translate(-500 -290.19)" fill="#e0e0e0" />
                    </g>
                  </g>
                  <g id="Group_593" data-name="Group 593" transform="translate(13.923 9.344)">
                    <g id="Group_592" data-name="Group 592">
                      <path id="Path_130" data-name="Path 130" d="M507.559,290.329c-.034,0-.059-3.621-.059-8.089s.025-8.089.059-8.089.059,3.621.059,8.089S507.593,290.329,507.559,290.329Z" transform="translate(-507.5 -274.15)" fill="#e0e0e0" />
                    </g>
                  </g>
                  <g id="Group_595" data-name="Group 595" transform="translate(20.427 9.344)">
                    <g id="Group_594" data-name="Group 594">
                      <path id="Path_131" data-name="Path 131" d="M515.229,290.329c-.034,0-.059-3.621-.059-8.089s.025-8.089.059-8.089.059,3.621.059,8.089S515.263,290.329,515.229,290.329Z" transform="translate(-515.17 -274.15)" fill="#e0e0e0" />
                    </g>
                  </g>
                  <g id="Group_597" data-name="Group 597" transform="translate(26.93 9.344)">
                    <g id="Group_596" data-name="Group 596">
                      <path id="Path_132" data-name="Path 132" d="M522.9,290.329c-.034,0-.059-3.621-.059-8.089s.025-8.089.059-8.089.059,3.621.059,8.089S522.933,290.329,522.9,290.329Z" transform="translate(-522.84 -274.15)" fill="#e0e0e0" />
                    </g>
                  </g>
                  <g id="Group_599" data-name="Group 599" transform="translate(33.434 9.344)">
                    <g id="Group_598" data-name="Group 598">
                      <path id="Path_133" data-name="Path 133" d="M530.569,290.329c-.034,0-.059-3.621-.059-8.089s.025-8.089.059-8.089.059,3.621.059,8.089S530.6,290.329,530.569,290.329Z" transform="translate(-530.51 -274.15)" fill="#e0e0e0" />
                    </g>
                  </g>
                  <g id="Group_601" data-name="Group 601" transform="translate(39.938 9.344)">
                    <g id="Group_600" data-name="Group 600">
                      <path id="Path_134" data-name="Path 134" d="M538.239,290.329c-.034,0-.059-3.621-.059-8.089s.025-8.089.059-8.089.059,3.621.059,8.089S538.273,290.329,538.239,290.329Z" transform="translate(-538.18 -274.15)" fill="#e0e0e0" />
                    </g>
                  </g>
                  <g id="Group_603" data-name="Group 603" transform="translate(7.564 12.024)">
                    <g id="Group_602" data-name="Group 602">
                      <path id="Path_135" data-name="Path 135" d="M532.433,277.31a1.658,1.658,0,0,1-.246.187c-.17.119-.415.3-.729.517-.644.441-1.569,1.085-2.722,1.882-2.315,1.586-5.562,3.8-9.251,6.292l-.051.034-.051-.017c-.89-.3-1.815-.611-2.764-.933-1.289-.432-2.535-.856-3.739-1.263l.1-.017c-2.451,1.645-4.681,3.137-6.512,4.367l-.008.008h-.009c-1.976.678-3.578,1.238-4.706,1.628-.56.187-.992.331-1.289.432-.144.042-.254.085-.339.11s-.119.034-.119.025.034-.025.11-.051.187-.076.331-.127l1.272-.475c1.111-.407,2.713-.984,4.681-1.7l-.017.009c1.832-1.246,4.045-2.747,6.487-4.409l.042-.034.051.017c1.2.407,2.459.823,3.739,1.263.95.322,1.874.636,2.764.933l-.1.017c3.688-2.484,6.953-4.672,9.293-6.224,1.17-.772,2.111-1.391,2.764-1.823.322-.2.577-.373.746-.483A1.739,1.739,0,0,1,532.433,277.31Z" transform="translate(-500 -277.31)" fill="#525bf4" />
                    </g>
                  </g>
                  <g id="Group_604" data-name="Group 604" transform="translate(13.584 22.606)">
                    <circle id="Ellipse_7" data-name="Ellipse 7" cx="0.399" cy="0.399" r="0.399" fill="#525bf4" />
                  </g>
                  <g id="Group_605" data-name="Group 605" transform="translate(20.088 18.214)">
                    <circle id="Ellipse_8" data-name="Ellipse 8" cx="0.399" cy="0.399" r="0.399" fill="#525bf4" />
                  </g>
                  <g id="Group_606" data-name="Group 606" transform="translate(26.591 20.367)">
                    <circle id="Ellipse_9" data-name="Ellipse 9" cx="0.399" cy="0.399" r="0.399" fill="#525bf4" />
                  </g>
                  <g id="Group_607" data-name="Group 607" transform="translate(33.163 16.034)">
                    <circle id="Ellipse_10" data-name="Ellipse 10" cx="0.399" cy="0.399" r="0.399" fill="#525bf4" />
                  </g>
                  <g id="Group_609" data-name="Group 609" transform="translate(7.402 14.025)">
                    <g id="Group_608" data-name="Group 608">
                      <path id="Path_136" data-name="Path 136" d="M532.4,288.65a1.029,1.029,0,0,1-.127-.068c-.085-.051-.2-.127-.348-.22-.305-.2-.746-.483-1.314-.848-1.136-.746-2.764-1.823-4.765-3.146h.1c-1.848,1.246-4.062,2.739-6.487,4.367l-.085.059-.059-.085c-1.068-1.442-2.23-3-3.417-4.613-1.068-1.441-2.1-2.841-3.086-4.172l.17.009c-2.484,2.5-4.715,4.748-6.521,6.563l-.017.017-.017.008c-2.035.661-3.689,1.2-4.833,1.569-.568.178-1.009.322-1.323.415l-.348.1c-.076.025-.119.034-.119.025s.034-.017.11-.051.2-.068.339-.127c.305-.11.746-.263,1.306-.458,1.145-.39,2.781-.95,4.808-1.645l-.034.017c1.8-1.823,4.011-4.087,6.487-6.605l.093-.093.076.1c.984,1.331,2.018,2.722,3.086,4.163,1.187,1.611,2.34,3.171,3.409,4.613l-.153-.025c2.434-1.62,4.655-3.1,6.512-4.333l.051-.034.051.034c1.984,1.348,3.6,2.442,4.723,3.205.551.382.984.678,1.289.89.144.1.254.178.331.237C532.371,288.616,532.4,288.641,532.4,288.65Z" transform="translate(-499.81 -279.67)" fill="#455a64" />
                    </g>
                  </g>
                  <g id="Group_610" data-name="Group 610" transform="translate(20.088 13.821)">
                    <circle id="Ellipse_11" data-name="Ellipse 11" cx="0.399" cy="0.399" r="0.399" fill="#455a64" />
                  </g>
                  <g id="Group_611" data-name="Group 611" transform="translate(13.584 20.41)">
                    <circle id="Ellipse_12" data-name="Ellipse 12" cx="0.399" cy="0.399" r="0.399" fill="#455a64" />
                  </g>
                  <g id="Group_612" data-name="Group 612" transform="translate(26.591 22.606)">
                    <circle id="Ellipse_13" data-name="Ellipse 13" cx="0.399" cy="0.399" r="0.399" fill="#455a64" />
                  </g>
                  <g id="Group_613" data-name="Group 613" transform="translate(33.095 18.612)">
                    <circle id="Ellipse_14" data-name="Ellipse 14" cx="0.399" cy="0.399" r="0.399" fill="#455a64" />
                  </g>
                  <g id="Group_615" data-name="Group 615" transform="translate(10.014 27.032)">
                    <g id="Group_614" data-name="Group 614">
                      <path id="Path_137" data-name="Path 137" d="M504.145,295.069a3.348,3.348,0,0,1-1.255,0,1.676,1.676,0,0,1,.627-.059A1.914,1.914,0,0,1,504.145,295.069Z" transform="translate(-502.89 -295.01)" fill="#e0e0e0" />
                    </g>
                  </g>
                  <g id="Group_617" data-name="Group 617" transform="translate(16.662 27.032)">
                    <g id="Group_616" data-name="Group 616">
                      <path id="Path_138" data-name="Path 138" d="M511.985,295.069a3.348,3.348,0,0,1-1.255,0,1.676,1.676,0,0,1,.627-.059A1.915,1.915,0,0,1,511.985,295.069Z" transform="translate(-510.73 -295.01)" fill="#e0e0e0" />
                    </g>
                  </g>
                  <g id="Group_619" data-name="Group 619" transform="translate(23.31 27.032)">
                    <g id="Group_618" data-name="Group 618">
                      <path id="Path_139" data-name="Path 139" d="M519.825,295.069a3.348,3.348,0,0,1-1.255,0,1.676,1.676,0,0,1,.628-.059A1.914,1.914,0,0,1,519.825,295.069Z" transform="translate(-518.57 -295.01)" fill="#e0e0e0" />
                    </g>
                  </g>
                  <g id="Group_621" data-name="Group 621" transform="translate(29.958 27.032)">
                    <g id="Group_620" data-name="Group 620">
                      <path id="Path_140" data-name="Path 140" d="M527.665,295.069a3.348,3.348,0,0,1-1.255,0,1.676,1.676,0,0,1,.628-.059A1.959,1.959,0,0,1,527.665,295.069Z" transform="translate(-526.41 -295.01)" fill="#e0e0e0" />
                    </g>
                  </g>
                  <g id="Group_623" data-name="Group 623" transform="translate(36.597 27.032)">
                    <g id="Group_622" data-name="Group 622">
                      <path id="Path_141" data-name="Path 141" d="M535.495,295.069a3.348,3.348,0,0,1-1.255,0,1.676,1.676,0,0,1,.628-.059A1.913,1.913,0,0,1,535.495,295.069Z" transform="translate(-534.24 -295.01)" fill="#e0e0e0" />
                    </g>
                  </g>
                  <g id="Group_638" data-name="Group 638" transform="translate(5.63 9.819)">
                    <g id="Group_625" data-name="Group 625" transform="translate(0 15.314)">
                      <g id="Group_624" data-name="Group 624">
                        <path id="Path_142" data-name="Path 142" d="M498.738,292.829a2.211,2.211,0,1,1-.509-.059A1.126,1.126,0,0,1,498.738,292.829Z" transform="translate(-497.72 -292.77)" fill="#e0e0e0" />
                      </g>
                    </g>
                    <g id="Group_627" data-name="Group 627" transform="translate(0 12.761)">
                      <g id="Group_626" data-name="Group 626">
                        <path id="Path_143" data-name="Path 143" d="M498.738,289.819a2.21,2.21,0,1,1-.509-.059A1.126,1.126,0,0,1,498.738,289.819Z" transform="translate(-497.72 -289.76)" fill="#e0e0e0" />
                      </g>
                    </g>
                    <g id="Group_629" data-name="Group 629" transform="translate(0 10.209)">
                      <g id="Group_628" data-name="Group 628">
                        <path id="Path_144" data-name="Path 144" d="M498.738,286.809a2.209,2.209,0,1,1-.509-.059A1.127,1.127,0,0,1,498.738,286.809Z" transform="translate(-497.72 -286.75)" fill="#e0e0e0" />
                      </g>
                    </g>
                    <g id="Group_631" data-name="Group 631" transform="translate(0 7.657)">
                      <g id="Group_630" data-name="Group 630">
                        <path id="Path_145" data-name="Path 145" d="M498.738,283.8a2.21,2.21,0,1,1-.509-.059A1.127,1.127,0,0,1,498.738,283.8Z" transform="translate(-497.72 -283.74)" fill="#e0e0e0" />
                      </g>
                    </g>
                    <g id="Group_633" data-name="Group 633" transform="translate(0 5.105)">
                      <g id="Group_632" data-name="Group 632">
                        <path id="Path_146" data-name="Path 146" d="M498.738,280.789a2.21,2.21,0,1,1-.509-.059A1.127,1.127,0,0,1,498.738,280.789Z" transform="translate(-497.72 -280.73)" fill="#e0e0e0" />
                      </g>
                    </g>
                    <g id="Group_635" data-name="Group 635" transform="translate(0 2.552)">
                      <g id="Group_634" data-name="Group 634">
                        <path id="Path_147" data-name="Path 147" d="M498.738,277.779a2.209,2.209,0,1,1-.509-.059A1.127,1.127,0,0,1,498.738,277.779Z" transform="translate(-497.72 -277.72)" fill="#e0e0e0" />
                      </g>
                    </g>
                    <g id="Group_637" data-name="Group 637">
                      <g id="Group_636" data-name="Group 636">
                        <path id="Path_148" data-name="Path 148" d="M498.738,274.769a2.21,2.21,0,1,1-.509-.059A1.127,1.127,0,0,1,498.738,274.769Z" transform="translate(-497.72 -274.71)" fill="#e0e0e0" />
                      </g>
                    </g>
                  </g>
                  <g id="Group_639" data-name="Group 639" transform="translate(7.394 3.824)">
                    <rect id="Rectangle_91" data-name="Rectangle 91" width="9.319" height="0.661" fill="#525bf4" />
                  </g>
                  <g id="Group_641" data-name="Group 641" transform="translate(7.335 5.698)">
                    <g id="Group_640" data-name="Group 640">
                      <path id="Path_149" data-name="Path 149" d="M525.745,269.909c0,.034-5.825.059-13.007.059s-13.007-.025-13.007-.059,5.825-.059,13.007-.059S525.745,269.875,525.745,269.909Z" transform="translate(-499.73 -269.85)" fill="#263238" />
                    </g>
                  </g>
                  <g id="Group_643" data-name="Group 643" transform="translate(7.971 30.22)">
                    <g id="Group_642" data-name="Group 642">
                      <path id="Path_150" data-name="Path 150" d="M516.353,298.829c0,.034-3.553.059-7.937.059s-7.937-.025-7.937-.059,3.553-.059,7.937-.059S516.353,298.8,516.353,298.829Z" transform="translate(-500.48 -298.77)" fill="#263238" />
                    </g>
                  </g>
                  <g id="Group_645" data-name="Group 645" transform="translate(8.395 31.416)">
                    <g id="Group_644" data-name="Group 644">
                      <path id="Path_151" data-name="Path 151" d="M504.372,300.239a24.256,24.256,0,0,1-3.392,0,11.9,11.9,0,0,1,1.7-.059A16.1,16.1,0,0,1,504.372,300.239Z" transform="translate(-500.98 -300.18)" fill="#263238" />
                    </g>
                  </g>
                  <g id="Group_647" data-name="Group 647" transform="translate(26.947 30.008)">
                    <g id="Group_646" data-name="Group 646">
                      <path id="Path_152" data-name="Path 152" d="M534.138,298.579c0,.034-2.527.059-5.639.059s-5.639-.025-5.639-.059,2.527-.059,5.639-.059S534.138,298.545,534.138,298.579Z" transform="translate(-522.86 -298.52)" fill="#263238" />
                    </g>
                  </g>
                  <g id="Group_649" data-name="Group 649" transform="translate(28.567 30.788)">
                    <g id="Group_648" data-name="Group 648">
                      <path id="Path_153" data-name="Path 153" d="M528.3,299.5a26.233,26.233,0,0,1-3.527,0,26.233,26.233,0,0,1,3.527,0Z" transform="translate(-524.77 -299.44)" fill="#263238" />
                    </g>
                  </g>
                </g>
              </g>
              <g id="Group_682" data-name="Group 682" transform="translate(0 37.674)">
                <g id="Group_652" data-name="Group 652" transform="translate(0 13.219)">
                  <path id="Path_154" data-name="Path 154" d="M202.05,442.807,216.058,318.89l11.888,4.189L208.528,442.807Z" transform="translate(-202.05 -318.89)" fill="#263238" />
                </g>
                <g id="Group_653" data-name="Group 653" transform="translate(0 1.467)">
                  <rect id="Rectangle_92" data-name="Rectangle 92" width="300.762" height="15.678" fill="#455a64" />
                </g>
                <g id="Group_659" data-name="Group 659" transform="translate(34.51 5.071)">
                  <g id="Group_658" data-name="Group 658">
                    <path id="Path_157" data-name="Path 157" d="M332.376,318.23a4.146,4.146,0,0,1,.56.2,10.839,10.839,0,0,0,1.653.441,9.733,9.733,0,0,0,6.275-.984l-.017.2a14.9,14.9,0,0,0-7.776-3.027,15.115,15.115,0,0,0-10.116,2.629,1.591,1.591,0,0,0-.78,1.018,1.083,1.083,0,0,0,.67,1.009,4.577,4.577,0,0,0,1.391.483c.509.119,1.018.229,1.526.348,2.052.458,4.172.907,6.359,1.229a37.864,37.864,0,0,0,6.767.458,25.5,25.5,0,0,0,3.477-.331c.577-.1,1.153-.212,1.73-.365a8.55,8.55,0,0,0,1.67-.543,5.492,5.492,0,0,0,2.171-1.569,2.465,2.465,0,0,0,.415-2.408,4.056,4.056,0,0,0-1.7-1.9,13.466,13.466,0,0,0-2.383-1.162,32.625,32.625,0,0,0-9.955-2.069,41.079,41.079,0,0,0-16.993,2.637,42.242,42.242,0,0,0-10.235,5.588c-1.094.848-1.933,1.509-2.476,2.01-.263.22-.475.4-.627.534-.144.119-.22.178-.229.17s.059-.076.2-.2c.153-.144.356-.331.6-.568.517-.517,1.348-1.2,2.425-2.077a40.786,40.786,0,0,1,10.226-5.741,41.243,41.243,0,0,1,17.128-2.747,32.848,32.848,0,0,1,10.09,2.069,13.7,13.7,0,0,1,2.459,1.2,4.6,4.6,0,0,1,1.882,2.12,2.926,2.926,0,0,1-.483,2.832,5.971,5.971,0,0,1-2.34,1.7,9.335,9.335,0,0,1-1.747.568c-.585.153-1.17.271-1.764.373a25.074,25.074,0,0,1-3.536.339,37.776,37.776,0,0,1-6.843-.475c-2.213-.331-4.333-.789-6.393-1.255-.509-.119-1.026-.237-1.526-.348a5.126,5.126,0,0,1-1.5-.534,1.94,1.94,0,0,1-.627-.551,1.253,1.253,0,0,1-.229-.848,1.965,1.965,0,0,1,.95-1.289,15.45,15.45,0,0,1,10.379-2.637,15.073,15.073,0,0,1,7.894,3.163l.144.119-.161.085a9.8,9.8,0,0,1-6.41.9A7.384,7.384,0,0,1,332.376,318.23Z" transform="translate(-303.749 -311.43)" fill="#263238" />
                  </g>
                </g>
                <g id="Group_661" data-name="Group 661" transform="translate(96.817 2.111)">
                  <g id="Group_660" data-name="Group 660">
                    <path id="Path_158" data-name="Path 158" d="M389.678,307.94a2.3,2.3,0,0,1,.356.789,6.109,6.109,0,0,1,.237,2.349,11.6,11.6,0,0,1-3.493,6.843,17.68,17.68,0,0,1-3.265,2.773,14.7,14.7,0,0,1-3.146,1.526,14.368,14.368,0,0,1-2.281.568,3.6,3.6,0,0,1-.856.076c-.017-.085,1.2-.246,3.036-.941a15.5,15.5,0,0,0,3.027-1.569,18.851,18.851,0,0,0,3.163-2.73,11.889,11.889,0,0,0,3.5-6.571A8.65,8.65,0,0,0,389.678,307.94Z" transform="translate(-377.23 -307.94)" fill="#263238" />
                  </g>
                </g>
                <g id="Group_663" data-name="Group 663" transform="translate(121.373 5.987)">
                  <g id="Group_662" data-name="Group 662">
                    <path id="Path_159" data-name="Path 159" d="M458.448,322.846a20.95,20.95,0,0,0-1.645-1.6,11.756,11.756,0,0,0-5.6-2.527,12.374,12.374,0,0,0-4.265.127,18.174,18.174,0,0,1-5,.458,10.01,10.01,0,0,1-4.977-2.442,17.14,17.14,0,0,0-5-2.968,18.4,18.4,0,0,0-11.116-.17,26.475,26.475,0,0,0-8.225,4.163,44.691,44.691,0,0,0-4.765,4.07c-.5.5-.907.9-1.213,1.2a3.273,3.273,0,0,1-.441.407,3.608,3.608,0,0,1,.373-.466c.254-.3.644-.721,1.162-1.263a39.586,39.586,0,0,1,4.706-4.2,26.186,26.186,0,0,1,8.3-4.307,18.592,18.592,0,0,1,11.379.144,17.448,17.448,0,0,1,5.122,3.044A9.664,9.664,0,0,0,442,318.9a18.5,18.5,0,0,0,4.893-.415,12.5,12.5,0,0,1,4.375-.068,11.482,11.482,0,0,1,5.664,2.7A7.442,7.442,0,0,1,458.448,322.846Z" transform="translate(-406.19 -312.511)" fill="#263238" />
                  </g>
                </g>
                <g id="Group_665" data-name="Group 665" transform="translate(174.503 1.611)">
                  <g id="Group_664" data-name="Group 664">
                    <path id="Path_160" data-name="Path 160" d="M525.449,307.358a4.644,4.644,0,0,1-.39.534c-.28.331-.645.865-1.221,1.458A33.353,33.353,0,0,1,518.606,314a42.078,42.078,0,0,1-9.081,5.037,37.2,37.2,0,0,1-5.851,1.865,29.869,29.869,0,0,1-6.546.839,32.495,32.495,0,0,1-12.431-2.586,41.621,41.621,0,0,1-9.03-5.122,35.267,35.267,0,0,1-5.2-4.689,18.839,18.839,0,0,1-1.229-1.458,2.915,2.915,0,0,1-.39-.534,4.3,4.3,0,0,1,.466.475c.3.314.7.823,1.28,1.391a37.915,37.915,0,0,0,5.257,4.553,42.276,42.276,0,0,0,9,4.994,32.462,32.462,0,0,0,12.27,2.518,30.348,30.348,0,0,0,6.461-.822,37.43,37.43,0,0,0,5.8-1.823,42.6,42.6,0,0,0,9.047-4.91,36.153,36.153,0,0,0,5.291-4.511c.594-.568.984-1.085,1.28-1.4A4.048,4.048,0,0,1,525.449,307.358Z" transform="translate(-468.848 -307.35)" fill="#263238" />
                  </g>
                </g>
                <g id="Group_667" data-name="Group 667" transform="translate(187.096 1.465)">
                  <g id="Group_666" data-name="Group 666">
                    <path id="Path_161" data-name="Path 161" d="M519.5,307.18c.009.017-.093.093-.305.246s-.517.373-.924.627a38.943,38.943,0,0,1-3.587,2.035,37.862,37.862,0,0,1-5.673,2.306,24.851,24.851,0,0,1-7.4,1.263,30.363,30.363,0,0,1-13.143-3.324,28.378,28.378,0,0,1-3.561-2.094c-.407-.271-.7-.509-.907-.661s-.305-.246-.3-.263a11.778,11.778,0,0,1,1.3.78,34.956,34.956,0,0,0,3.6,1.95,31.672,31.672,0,0,0,13.007,3.163A25.476,25.476,0,0,0,508.892,312a41.367,41.367,0,0,0,5.656-2.2c1.56-.746,2.8-1.416,3.638-1.9A11.943,11.943,0,0,1,519.5,307.18Z" transform="translate(-483.699 -307.178)" fill="#263238" />
                  </g>
                </g>
                <g id="Group_669" data-name="Group 669" transform="translate(223.023 2.631)">
                  <g id="Group_668" data-name="Group 668">
                    <path id="Path_162" data-name="Path 162" d="M562.387,309a1.3,1.3,0,0,1-.415,0c-.322-.017-.7-.042-1.17-.076-.263-.017-.543-.042-.848-.068-.314,0-.653-.008-1.009-.008-.721-.008-1.543-.034-2.459.042l-1.425.076c-.492.059-1.009.119-1.535.178-1.068.093-2.188.348-3.375.543a50.892,50.892,0,0,0-7.5,2.239,52.075,52.075,0,0,0-7.021,3.451c-1.009.653-2,1.23-2.858,1.882-.432.314-.856.611-1.255.907-.382.314-.755.611-1.111.9-.721.56-1.323,1.128-1.857,1.611-.263.237-.517.466-.746.678-.212.22-.407.424-.594.611-.331.331-.6.611-.831.831a1.731,1.731,0,0,1-.314.271,1.652,1.652,0,0,1,.246-.331c.212-.246.466-.534.772-.89.178-.2.365-.407.568-.636.229-.22.466-.458.729-.712.526-.509,1.111-1.094,1.823-1.67.356-.3.721-.611,1.1-.933.407-.3.822-.61,1.255-.933.848-.67,1.84-1.263,2.858-1.933a49.952,49.952,0,0,1,7.072-3.519,48.617,48.617,0,0,1,7.572-2.222c1.2-.187,2.332-.432,3.409-.509q.8-.076,1.552-.153l1.442-.051a23.515,23.515,0,0,1,2.476.025c.356.017.7.034,1.018.042.305.034.585.068.848.1.466.059.848.119,1.17.161A1.251,1.251,0,0,1,562.387,309Z" transform="translate(-526.069 -308.552)" fill="#263238" />
                  </g>
                </g>
                <g id="Group_671" data-name="Group 671" transform="translate(253.549)">
                  <g id="Group_670" data-name="Group 670">
                    <path id="Path_163" data-name="Path 163" d="M583.209,305.45c.017,0,.017.1.008.3a8.213,8.213,0,0,1-.068.848,11.692,11.692,0,0,1-.839,3.01,17.646,17.646,0,0,1-6.69,7.691,34.566,34.566,0,0,1-9.429,4.019c-1.263.356-2.289.611-3,.78a4.845,4.845,0,0,1-1.119.212,5.408,5.408,0,0,1,1.077-.382c.78-.237,1.772-.551,2.959-.916a38.228,38.228,0,0,0,9.276-4.1,16.169,16.169,0,0,0,7.826-11.464Z" transform="translate(-562.07 -305.45)" fill="#263238" />
                  </g>
                </g>
                <g id="Group_673" data-name="Group 673" transform="translate(107.967 3.123)">
                  <g id="Group_672" data-name="Group 672">
                    <path id="Path_164" data-name="Path 164" d="M462.488,322.019a16.7,16.7,0,0,0-1.976-2.349,18.273,18.273,0,0,0-7.224-4.036,39.534,39.534,0,0,0-5.639-1.246c-2.052-.331-4.274-.585-6.588-.984a54.216,54.216,0,0,1-7.157-1.8,46.856,46.856,0,0,0-7.631-1.84,31.785,31.785,0,0,0-14.873,2.052,54.29,54.29,0,0,0-6.054,2.722c-1.857.95-3.553,1.891-5.1,2.756-3.086,1.747-5.537,3.231-7.233,4.265-.823.5-1.467.9-1.95,1.2q-.305.178-.509.305a.784.784,0,0,1-.178.093.728.728,0,0,1,.161-.127c.127-.093.288-.2.483-.339.432-.3,1.068-.729,1.908-1.263,1.67-1.077,4.1-2.6,7.173-4.392,1.535-.89,3.231-1.848,5.088-2.815a52.7,52.7,0,0,1,6.08-2.773,31.958,31.958,0,0,1,15.068-2.111,46.536,46.536,0,0,1,7.708,1.865,56.02,56.02,0,0,0,7.106,1.806c2.3.407,4.519.678,6.58,1.026a39.161,39.161,0,0,1,5.673,1.306,17.924,17.924,0,0,1,7.25,4.214A9.366,9.366,0,0,1,462.488,322.019Z" transform="translate(-390.38 -309.133)" fill="#263238" />
                  </g>
                </g>
                <g id="Group_675" data-name="Group 675" transform="translate(81.325 1.704)">
                  <g id="Group_674" data-name="Group 674">
                    <path id="Path_165" data-name="Path 165" d="M376.97,307.46a3.327,3.327,0,0,1-.051,1,11.461,11.461,0,0,1-.7,2.646,15.156,15.156,0,0,1-5.512,7.046,36.612,36.612,0,0,1-8.166,3.756c-1.094.365-1.984.619-2.6.78a4.521,4.521,0,0,1-.975.2,4.339,4.339,0,0,1,.933-.365c.687-.246,1.543-.551,2.552-.916a42.2,42.2,0,0,0,8.03-3.833,15.575,15.575,0,0,0,5.461-6.8A22.009,22.009,0,0,0,376.97,307.46Z" transform="translate(-358.96 -307.46)" fill="#263238" />
                  </g>
                </g>
                <g id="Group_677" data-name="Group 677" transform="translate(16.119 1.709)">
                  <g id="Group_676" data-name="Group 676">
                    <path id="Path_166" data-name="Path 166" d="M282.06,322.9a6.033,6.033,0,0,1,.873-.839c.577-.526,1.45-1.238,2.552-2.086a73.7,73.7,0,0,1,9.141-5.893c3.722-2.052,7.207-3.689,9.734-4.842,1.263-.577,2.289-1.035,3-1.34a5.78,5.78,0,0,1,1.128-.432,6.458,6.458,0,0,1-1.051.594c-.772.382-1.755.882-2.942,1.467-2.493,1.23-5.944,2.908-9.65,4.952a88.825,88.825,0,0,0-9.166,5.757c-1.119.814-2.018,1.492-2.629,1.967A7.172,7.172,0,0,1,282.06,322.9Z" transform="translate(-282.059 -307.465)" fill="#263238" />
                  </g>
                </g>
                <g id="Group_681" data-name="Group 681" transform="translate(189.649 1.187)">
                  <g id="Group_680" data-name="Group 680">
                    <path id="Path_168" data-name="Path 168" d="M553.629,307.07c0,.119-14.983.22-33.459.22s-33.46-.1-33.46-.22,14.975-.22,33.46-.22S553.629,306.943,553.629,307.07Z" transform="translate(-486.71 -306.85)" fill="#263238" />
                  </g>
                </g>
              </g>
              <g id="Group_692" data-name="Group 692" transform="translate(65.223 17.665)">
                <g id="Group_685" data-name="Group 685" transform="translate(2.9 14.437)">
                  <g id="Group_683" data-name="Group 683">
                    <rect id="Rectangle_93" data-name="Rectangle 93" width="48.307" height="7.216" fill="#263238" />
                  </g>
                  <g id="Group_684" data-name="Group 684" transform="translate(0 1.67)">
                    <rect id="Rectangle_94" data-name="Rectangle 94" width="12.77" height="3.884" fill="#e0e0e0" />
                  </g>
                </g>
                <g id="Group_688" data-name="Group 688" transform="translate(7.208 7.221)">
                  <g id="Group_686" data-name="Group 686">
                    <rect id="Rectangle_95" data-name="Rectangle 95" width="48.307" height="7.216" fill="#e8505b" />
                  </g>
                  <g id="Group_687" data-name="Group 687" transform="translate(15.551 1.73)">
                    <rect id="Rectangle_96" data-name="Rectangle 96" width="32.764" height="3.612" fill="#e0e0e0" />
                  </g>
                </g>
                <g id="Group_691" data-name="Group 691" transform="translate(0 0)">
                  <g id="Group_689" data-name="Group 689">
                    <rect id="Rectangle_97" data-name="Rectangle 97" width="48.307" height="7.216" transform="translate(48.307 7.216) rotate(180)" fill="#263238" />
                  </g>
                  <g id="Group_690" data-name="Group 690" transform="translate(35.536 1.67)">
                    <rect id="Rectangle_98" data-name="Rectangle 98" width="12.77" height="3.884" transform="translate(12.77 3.884) rotate(180)" fill="#e0e0e0" />
                  </g>
                </g>
              </g>
            </g>
            <g id="sit" transform="translate(210.704 51.918)">
              <g id="Group_825" data-name="Group 825">
                <g id="Group_720" data-name="Group 720" transform="translate(31.569 197.058)">
                  <g id="Group_719" data-name="Group 719">
                    <g id="Group_712" data-name="Group 712">
                      <g id="Group_694" data-name="Group 694" transform="translate(0.07)">
                        <g id="Group_693" data-name="Group 693">
                          <path id="Path_169" data-name="Path 169" d="M539.071,432.151l5.757,14.3s-12.6,10.659-11.795,13.406l28.134-10.175L553.1,427.03Z" transform="translate(-532.996 -427.03)" fill="#e8505b" />
                        </g>
                      </g>
                      <g id="Group_697" data-name="Group 697" transform="translate(19.092 15.044)">
                        <g id="Group_696" data-name="Group 696">
                          <g id="Group_695" data-name="Group 695">
                            <path id="Path_170" data-name="Path 170" d="M556.5,444.787a1.219,1.219,0,0,1,1.272,1.026,1.171,1.171,0,0,1-1,1.263,1.3,1.3,0,0,1-1.34-1.094c-.009-.6.594-1.289,1.187-1.2" transform="translate(-555.43 -444.772)" fill="#e0e0e0" />
                          </g>
                        </g>
                      </g>
                      <g id="Group_699" data-name="Group 699" transform="translate(0 20.401)" opacity="0.6">
                        <g id="Group_698" data-name="Group 698">
                          <path id="Path_171" data-name="Path 171" d="M561.155,453.345l-.9-2.255-26.634,10.6s-1.051,1-.594,1.832Z" transform="translate(-532.914 -451.09)" fill="#fff" />
                        </g>
                      </g>
                      <g id="Group_702" data-name="Group 702" transform="translate(11.388 19.029)">
                        <g id="Group_701" data-name="Group 701">
                          <g id="Group_700" data-name="Group 700">
                            <path id="Path_172" data-name="Path 172" d="M546.349,449.886c.051.136.763-.051,1.6.144.839.178,1.408.644,1.509.543.119-.076-.382-.822-1.4-1.043S546.273,449.775,546.349,449.886Z" transform="translate(-546.344 -449.471)" fill="#263238" />
                          </g>
                        </g>
                      </g>
                      <g id="Group_705" data-name="Group 705" transform="translate(8.882 21.565)">
                        <g id="Group_704" data-name="Group 704">
                          <g id="Group_703" data-name="Group 703">
                            <path id="Path_173" data-name="Path 173" d="M543.39,452.63c.017.144.7.161,1.374.577.687.39,1.034.975,1.17.924.136-.025-.059-.865-.907-1.357C544.178,452.265,543.347,452.5,543.39,452.63Z" transform="translate(-543.388 -452.462)" fill="#263238" />
                          </g>
                        </g>
                      </g>
                      <g id="Group_708" data-name="Group 708" transform="translate(6.55 23.853)">
                        <g id="Group_707" data-name="Group 707">
                          <g id="Group_706" data-name="Group 706">
                            <path id="Path_174" data-name="Path 174" d="M542.573,457.178c.136-.017.076-.772-.543-1.4s-1.374-.7-1.391-.56c-.034.144.526.39,1.034.916S542.437,457.212,542.573,457.178Z" transform="translate(-540.638 -455.161)" fill="#263238" />
                          </g>
                        </g>
                      </g>
                      <g id="Group_711" data-name="Group 711" transform="translate(10.428 14.892)">
                        <g id="Group_710" data-name="Group 710">
                          <g id="Group_709" data-name="Group 709">
                            <path id="Path_175" data-name="Path 175" d="M545.227,445.744c.1.1.661-.314,1.458-.543.789-.246,1.492-.2,1.518-.348.042-.127-.729-.424-1.662-.144S545.117,445.659,545.227,445.744Z" transform="translate(-545.212 -444.593)" fill="#263238" />
                          </g>
                        </g>
                      </g>
                    </g>
                    <g id="Group_715" data-name="Group 715" transform="translate(9.332 9.539)">
                      <g id="Group_714" data-name="Group 714">
                        <g id="Group_713" data-name="Group 713">
                          <path id="Path_176" data-name="Path 176" d="M543.92,441.875a2.752,2.752,0,0,0,1.408-.517,6.862,6.862,0,0,0,1.314-.984,7.8,7.8,0,0,0,.653-.721,1.826,1.826,0,0,0,.305-.483.631.631,0,0,0-.2-.755.805.805,0,0,0-.712-.093,1.837,1.837,0,0,0-.5.263,4.667,4.667,0,0,0-.763.619,5.432,5.432,0,0,0-.992,1.323,2.488,2.488,0,0,0-.415,1.441c.076.025.237-.526.721-1.263a5.73,5.73,0,0,1,1-1.162,5.332,5.332,0,0,1,.721-.543c.271-.187.526-.28.644-.178.042.042.059.059.025.17a1.374,1.374,0,0,1-.229.356,6.9,6.9,0,0,1-.594.7,8.529,8.529,0,0,1-1.17,1.018A10.1,10.1,0,0,0,543.92,441.875Z" transform="translate(-543.919 -438.279)" fill="#263238" />
                        </g>
                      </g>
                    </g>
                    <g id="Group_718" data-name="Group 718" transform="translate(6.442 9.153)">
                      <g id="Group_717" data-name="Group 717">
                        <g id="Group_716" data-name="Group 716">
                          <path id="Path_177" data-name="Path 177" d="M543.656,441.942a2.343,2.343,0,0,0-.119-1.492,4.932,4.932,0,0,0-.746-1.475,4.46,4.46,0,0,0-.67-.729,1.24,1.24,0,0,0-1.153-.39.718.718,0,0,0-.449.6,2.014,2.014,0,0,0,.042.568,3.317,3.317,0,0,0,.339.95,3.438,3.438,0,0,0,1.128,1.238c.814.534,1.45.483,1.441.432a11.869,11.869,0,0,1-1.229-.729,3.542,3.542,0,0,1-.924-1.162,3.232,3.232,0,0,1-.271-.822c-.068-.314-.042-.594.085-.611a.876.876,0,0,1,.661.28,5.043,5.043,0,0,1,1.408,1.942C543.529,441.373,543.571,441.942,543.656,441.942Z" transform="translate(-540.511 -437.824)" fill="#263238" />
                        </g>
                      </g>
                    </g>
                  </g>
                </g>
                <g id="Group_748" data-name="Group 748" transform="translate(58.05 194.438)">
                  <g id="Group_747" data-name="Group 747">
                    <g id="Group_740" data-name="Group 740">
                      <g id="Group_722" data-name="Group 722" transform="translate(0.07)">
                        <g id="Group_721" data-name="Group 721">
                          <path id="Path_178" data-name="Path 178" d="M570.3,429.062l5.757,14.3s-12.6,10.659-11.795,13.406L592.4,446.6l-8.064-22.657Z" transform="translate(-564.226 -423.94)" fill="#e8505b" />
                        </g>
                      </g>
                      <g id="Group_725" data-name="Group 725" transform="translate(19.092 15.044)">
                        <g id="Group_724" data-name="Group 724">
                          <g id="Group_723" data-name="Group 723">
                            <path id="Path_179" data-name="Path 179" d="M587.728,441.7A1.219,1.219,0,0,1,589,442.723a1.171,1.171,0,0,1-1,1.263,1.3,1.3,0,0,1-1.34-1.094c-.008-.6.594-1.289,1.187-1.2" transform="translate(-586.66 -441.682)" fill="#e0e0e0" />
                          </g>
                        </g>
                      </g>
                      <g id="Group_727" data-name="Group 727" transform="translate(0 20.401)" opacity="0.6">
                        <g id="Group_726" data-name="Group 726">
                          <path id="Path_180" data-name="Path 180" d="M592.385,450.256l-.9-2.255-26.634,10.6s-1.051,1-.594,1.832Z" transform="translate(-564.144 -448)" fill="#fff" />
                        </g>
                      </g>
                      <g id="Group_730" data-name="Group 730" transform="translate(11.388 19.033)">
                        <g id="Group_729" data-name="Group 729">
                          <g id="Group_728" data-name="Group 728">
                            <path id="Path_181" data-name="Path 181" d="M577.579,446.8c.051.136.763-.051,1.6.144.839.178,1.408.644,1.509.543.119-.076-.382-.823-1.4-1.043S577.5,446.686,577.579,446.8Z" transform="translate(-577.574 -446.387)" fill="#263238" />
                          </g>
                        </g>
                      </g>
                      <g id="Group_733" data-name="Group 733" transform="translate(8.882 21.567)">
                        <g id="Group_732" data-name="Group 732">
                          <g id="Group_731" data-name="Group 731">
                            <path id="Path_182" data-name="Path 182" d="M574.62,449.549c.017.144.7.161,1.374.577.687.39,1.034.975,1.17.924.136-.025-.059-.865-.907-1.357C575.408,449.176,574.577,449.413,574.62,449.549Z" transform="translate(-574.618 -449.375)" fill="#263238" />
                          </g>
                        </g>
                      </g>
                      <g id="Group_736" data-name="Group 736" transform="translate(6.55 23.862)">
                        <g id="Group_735" data-name="Group 735">
                          <g id="Group_734" data-name="Group 734">
                            <path id="Path_183" data-name="Path 183" d="M573.8,454.1c.136-.017.076-.772-.543-1.4s-1.374-.7-1.391-.56c-.034.144.526.39,1.034.916C573.413,453.564,573.667,454.132,573.8,454.1Z" transform="translate(-571.868 -452.081)" fill="#263238" />
                          </g>
                        </g>
                      </g>
                      <g id="Group_739" data-name="Group 739" transform="translate(10.43 14.892)">
                        <g id="Group_738" data-name="Group 738">
                          <g id="Group_737" data-name="Group 737">
                            <path id="Path_184" data-name="Path 184" d="M576.458,442.654c.1.1.661-.314,1.458-.543.789-.246,1.492-.2,1.518-.348.042-.127-.729-.424-1.662-.144S576.356,442.569,576.458,442.654Z" transform="translate(-576.444 -441.503)" fill="#263238" />
                          </g>
                        </g>
                      </g>
                    </g>
                    <g id="Group_743" data-name="Group 743" transform="translate(9.332 9.535)">
                      <g id="Group_742" data-name="Group 742">
                        <g id="Group_741" data-name="Group 741">
                          <path id="Path_185" data-name="Path 185" d="M575.15,438.784a2.751,2.751,0,0,0,1.408-.517,6.866,6.866,0,0,0,1.314-.984,7.8,7.8,0,0,0,.653-.721,1.824,1.824,0,0,0,.305-.483.63.63,0,0,0-.2-.755.78.78,0,0,0-.712-.093,1.835,1.835,0,0,0-.5.263,4.664,4.664,0,0,0-.763.619,5.432,5.432,0,0,0-.992,1.323,2.489,2.489,0,0,0-.415,1.442c.076.025.237-.526.721-1.263a5.73,5.73,0,0,1,1-1.162,5.328,5.328,0,0,1,.721-.543c.271-.187.526-.28.644-.178.042.042.059.059.025.17a1.375,1.375,0,0,1-.229.356,6.908,6.908,0,0,1-.594.7,8.531,8.531,0,0,1-1.17,1.018A13.3,13.3,0,0,0,575.15,438.784Z" transform="translate(-575.149 -435.185)" fill="#263238" />
                        </g>
                      </g>
                    </g>
                    <g id="Group_746" data-name="Group 746" transform="translate(6.442 9.153)">
                      <g id="Group_745" data-name="Group 745">
                        <g id="Group_744" data-name="Group 744">
                          <path id="Path_186" data-name="Path 186" d="M574.886,438.852a2.343,2.343,0,0,0-.119-1.492,4.93,4.93,0,0,0-.746-1.475,4.46,4.46,0,0,0-.67-.729,1.24,1.24,0,0,0-1.153-.39.719.719,0,0,0-.449.6,2.012,2.012,0,0,0,.042.568,3.32,3.32,0,0,0,.339.95,3.44,3.44,0,0,0,1.128,1.238c.814.534,1.45.483,1.442.432a11.877,11.877,0,0,1-1.23-.729,3.541,3.541,0,0,1-.924-1.162,3.228,3.228,0,0,1-.271-.823c-.068-.314-.042-.594.085-.611a.876.876,0,0,1,.661.28,5.039,5.039,0,0,1,1.408,1.942C574.767,438.292,574.8,438.86,574.886,438.852Z" transform="translate(-571.741 -434.734)" fill="#263238" />
                        </g>
                      </g>
                    </g>
                  </g>
                </g>
                <g id="Group_754" data-name="Group 754" transform="translate(56.337)">
                  <g id="Group_753" data-name="Group 753">
                    <g id="Group_750" data-name="Group 750" transform="translate(0.262 5.12)">
                      <g id="Group_749" data-name="Group 749">
                        <path id="Path_187" data-name="Path 187" d="M589.429,209.785l-2.688,22,4.18,10.15-16.535.585-3.434-7.928s-8.191-.924-8.513-6.173c-.212-3.349,5.189-27.753,5.189-27.753Z" transform="translate(-562.433 -200.67)" fill="#ffbe9d" />
                      </g>
                    </g>
                    <g id="Group_752" data-name="Group 752">
                      <g id="Group_751" data-name="Group 751">
                        <path id="Path_188" data-name="Path 188" d="M584.082,235.06a8.3,8.3,0,0,0,3.79-4.4c.857-2.2,1.408-4.5,2.383-6.648,1.026-2.256,2.5-4.282,3.51-6.538s1.526-4.918.543-7.19c0,0-.348-2.908-4.274-3.968,0,0-2.213-6.164-7.284-6.741-6.9-.789-6.665-2.815-11.167-4.536a4.371,4.371,0,0,0-5.842,2.1s-2.773,1.272-3.426,3.807c-.008.025-.008.042-.017.068a6.034,6.034,0,0,0,2.815,6.622c.28.17.56.331.839.492a5.875,5.875,0,0,1,2.12,1.7,5.011,5.011,0,0,1,.254,3.951,10.692,10.692,0,0,0-.628,4c.059.458.331,1,.789.984.551-.025.678-.763.848-1.289a2.323,2.323,0,0,1,3.917-.848,2.463,2.463,0,0,1,.1,3.07c-.755.992-2.086,1.611-2.569,2.781-.814,1.976,1.221,4.291,1.908,5.6a8.275,8.275,0,0,0,.822,1.331,19.59,19.59,0,0,0,5.766,5.02,5.288,5.288,0,0,0,5.41.271" transform="translate(-562.124 -194.632)" fill="#263238" />
                      </g>
                    </g>
                  </g>
                </g>
                <g id="Group_775" data-name="Group 775" transform="translate(0 50.442)">
                  <g id="Group_759" data-name="Group 759">
                    <g id="Group_758" data-name="Group 758">
                      <g id="Group_757" data-name="Group 757">
                        <g id="Group_756" data-name="Group 756">
                          <g id="Group_755" data-name="Group 755">
                            <path id="Path_189" data-name="Path 189" d="M505.949,270.706h0c-2.9-1.238-4.774-2.357-6.716-3.061-2.4-.873-3.324-1.4-3.129-1.84.254-.585.127-.373,4.715.594,0,0-6.521-1.433-4.867-2.23.738-.356,6.164.78,6.164.78-4.452-.509-6.046-1.408-5.927-2.077.161-.967,6.75.458,6.75.458a50.554,50.554,0,0,1-5.876-1.441c-.831-.449.229-.873,1.2-.984,1.094-.119,7.538.11,9.4.076a1.1,1.1,0,0,0,1.2-1.153,6.954,6.954,0,0,1,1.017-5.52c.568-.509,1.458.1,1.28.924a9.977,9.977,0,0,0,.042,2.739,6.723,6.723,0,0,0,.6,2.1c.373.763.721,1.4,1.026,1.925l9.242,10.107-3.994,9.59Z" transform="translate(-495.683 -254.121)" fill="#ffbe9d" />
                          </g>
                        </g>
                      </g>
                    </g>
                  </g>
                  <g id="Group_764" data-name="Group 764" transform="translate(1.642 11.419)">
                    <g id="Group_763" data-name="Group 763">
                      <g id="Group_762" data-name="Group 762">
                        <g id="Group_761" data-name="Group 761">
                          <g id="Group_760" data-name="Group 760">
                            <path id="Path_190" data-name="Path 190" d="M503.852,269.091a11.722,11.722,0,0,1-3.163-.534,12.262,12.262,0,0,1-3.069-.941,12.479,12.479,0,0,1,3.163.534A11.763,11.763,0,0,1,503.852,269.091Z" transform="translate(-497.619 -267.588)" fill="#eb996e" />
                          </g>
                        </g>
                      </g>
                    </g>
                  </g>
                  <g id="Group_769" data-name="Group 769" transform="translate(1.693 9.992)">
                    <g id="Group_768" data-name="Group 768">
                      <g id="Group_767" data-name="Group 767">
                        <g id="Group_766" data-name="Group 766">
                          <g id="Group_765" data-name="Group 765">
                            <path id="Path_191" data-name="Path 191" d="M504.421,267.113a27.274,27.274,0,0,1-6.741-1.17,27.273,27.273,0,0,1,6.741,1.17Z" transform="translate(-497.68 -265.905)" fill="#eb996e" />
                          </g>
                        </g>
                      </g>
                    </g>
                  </g>
                  <g id="Group_774" data-name="Group 774" transform="translate(2.609 8.351)">
                    <g id="Group_773" data-name="Group 773">
                      <g id="Group_772" data-name="Group 772">
                        <g id="Group_771" data-name="Group 771">
                          <g id="Group_770" data-name="Group 770">
                            <path id="Path_192" data-name="Path 192" d="M505.153,265.03a24.51,24.51,0,0,1-6.393-1.018,24.51,24.51,0,0,1,6.393,1.018Z" transform="translate(-498.76 -263.969)" fill="#eb996e" />
                          </g>
                        </g>
                      </g>
                    </g>
                  </g>
                </g>
                <g id="Group_778" data-name="Group 778" transform="translate(14.901 108.881)">
                  <g id="Group_776" data-name="Group 776" transform="translate(0 0.008)">
                    <path id="Path_193" data-name="Path 193" d="M569.155,324.28l-39.522,5.961a19.281,19.281,0,0,0-13.245,8.5h0a19.242,19.242,0,0,0-2.366,15.9L534.89,421.2l18.849-7.14-11.574-58.838,61.144-4.1V324.28l-34.146-1.229" transform="translate(-513.256 -323.05)" fill="#263238" />
                  </g>
                  <g id="Group_777" data-name="Group 777" transform="translate(27.838)">
                    <path id="Path_194" data-name="Path 194" d="M601.985,324.278l-39.522,5.961a19.281,19.281,0,0,0-13.245,8.5h0a19.242,19.242,0,0,0-2.366,15.9l18.2,62.594,21.054-5.562L575,355.211l40.845-2.739V325.635l-13.855-2.595" transform="translate(-546.086 -323.04)" fill="#263238" />
                  </g>
                </g>
                <g id="Group_808" data-name="Group 808" transform="translate(13.198 43.073)">
                  <g id="Group_779" data-name="Group 779" transform="translate(0 17.738)">
                    <path id="Path_195" data-name="Path 195" d="M518.143,266.35c.814-.11,27.668,20.817,27.668,20.817l-17.137,6.826L511.3,276.754C511.3,276.763,510.2,267.444,518.143,266.35Z" transform="translate(-511.249 -266.35)" fill="#e8505b" />
                  </g>
                  <g id="Group_807" data-name="Group 807" transform="translate(15.985)">
                    <g id="Group_780" data-name="Group 780">
                      <path id="Path_196" data-name="Path 196" d="M607.839,254.952c-4.477-6.487-12.092-8.734-14.729-9.336a7.738,7.738,0,0,0-1.713-.187H568.554a15.978,15.978,0,0,0-11.1,4.418,54.7,54.7,0,0,0-8.776,11.3l-11.506,15.992L530.1,289.005l1.441,1.815a11.893,11.893,0,0,0,14.211,3.612c2.332-1.085,4.036-3.12,5.622-5.444,2.891-4.24,5.9-8.836,7.9-12.337l11.532,32.976-6.173,3.917,26,10.506L617.7,312.476l-7.479-26.634v.009s11.854,1.153,12.948-3.5S613.316,262.9,607.839,254.952Z" transform="translate(-530.1 -245.43)" fill="#e8505b" />
                    </g>
                    <g id="Group_782" data-name="Group 782" transform="translate(75.02 21.238)">
                      <g id="Group_781" data-name="Group 781">
                        <path id="Path_197" data-name="Path 197" d="M618.613,270.844a112.257,112.257,0,0,1,5.4,9.989c.221.441.865.059.661-.382a84.417,84.417,0,0,0-5.63-9.853c-.17-.28-.6-.025-.432.246Z" transform="translate(-618.574 -270.477)" fill="#fafafa" />
                      </g>
                    </g>
                    <g id="Group_784" data-name="Group 784" transform="translate(80.396 19.311)">
                      <g id="Group_783" data-name="Group 783">
                        <path id="Path_198" data-name="Path 198" d="M624.953,268.574a112.257,112.257,0,0,1,5.4,9.989c.22.441.865.059.661-.382a84.414,84.414,0,0,0-5.63-9.853.249.249,0,0,0-.432.246Z" transform="translate(-624.914 -268.205)" fill="#fafafa" />
                      </g>
                    </g>
                    <g id="Group_786" data-name="Group 786" transform="translate(69.853 22.103)">
                      <g id="Group_785" data-name="Group 785">
                        <path id="Path_199" data-name="Path 199" d="M612.489,271.754c1.145,4.426,2.043,8.92,2.773,13.44.1.627,1.068.365.958-.263a89.569,89.569,0,0,0-3.324-13.287.211.211,0,0,0-.407.11Z" transform="translate(-612.481 -271.497)" fill="#fafafa" />
                      </g>
                    </g>
                    <g id="Group_788" data-name="Group 788" transform="translate(57.603 23.327)">
                      <g id="Group_787" data-name="Group 787">
                        <path id="Path_200" data-name="Path 200" d="M598.047,273.245a53.736,53.736,0,0,1,2.23,13.321c.034.627,1,.636.975,0a44.517,44.517,0,0,0-2.73-13.448c-.1-.305-.577-.187-.475.127Z" transform="translate(-598.033 -272.941)" fill="#fafafa" />
                      </g>
                    </g>
                    <g id="Group_790" data-name="Group 790" transform="translate(63.748 21.431)">
                      <g id="Group_789" data-name="Group 789">
                        <path id="Path_201" data-name="Path 201" d="M609.164,283.708A60.006,60.006,0,0,1,606.2,271.15c-.059-.577-.933-.611-.924,0a25.188,25.188,0,0,0,3.4,12.761c.153.237.577.085.483-.2Z" transform="translate(-605.28 -270.705)" fill="#fafafa" />
                      </g>
                    </g>
                    <g id="Group_792" data-name="Group 792" transform="translate(52.148 23.747)">
                      <g id="Group_791" data-name="Group 791">
                        <path id="Path_202" data-name="Path 202" d="M591.6,273.677a57.621,57.621,0,0,0,1.84,12.668c.153.577.992.331.882-.246-.8-4.163-1.56-8.3-2.247-12.49-.034-.271-.483-.2-.475.068Z" transform="translate(-591.6 -273.436)" fill="#fafafa" />
                      </g>
                    </g>
                    <g id="Group_794" data-name="Group 794" transform="translate(45.578 24.513)">
                      <g id="Group_793" data-name="Group 793">
                        <path id="Path_203" data-name="Path 203" d="M583.859,274.56c1.145,4.647,1.984,9.31,2.824,14.016.076.441.856.348.8-.11a76.134,76.134,0,0,0-3.273-14,.18.18,0,0,0-.348.093Z" transform="translate(-583.852 -274.339)" fill="#fafafa" />
                      </g>
                    </g>
                    <g id="Group_796" data-name="Group 796" transform="translate(39.581 25.546)">
                      <g id="Group_795" data-name="Group 795">
                        <path id="Path_204" data-name="Path 204" d="M576.78,275.831a55.8,55.8,0,0,0,1.416,12.948c.119.458.763.254.7-.2a119.493,119.493,0,0,1-1.552-12.753.28.28,0,0,0-.56,0Z" transform="translate(-576.78 -275.557)" fill="#fafafa" />
                      </g>
                    </g>
                    <g id="Group_798" data-name="Group 798" transform="translate(20.132 25.534)">
                      <g id="Group_797" data-name="Group 797">
                        <path id="Path_205" data-name="Path 205" d="M554.112,287.674c3.6-3.358,6.741-7.64,9.734-11.549.263-.348-.322-.8-.611-.466-3.231,3.705-6.792,7.64-9.37,11.829-.093.153.119.305.246.186Z" transform="translate(-553.843 -275.544)" fill="#fafafa" />
                      </g>
                    </g>
                    <g id="Group_800" data-name="Group 800" transform="translate(15.839 23.061)">
                      <g id="Group_799" data-name="Group 799">
                        <path id="Path_206" data-name="Path 206" d="M549.136,283.458a40.907,40.907,0,0,0,8.344-10.252c.263-.441-.4-.814-.687-.4-2.484,3.621-5.249,6.911-7.962,10.353a.213.213,0,0,0,.305.3Z" transform="translate(-548.78 -272.627)" fill="#fafafa" />
                      </g>
                    </g>
                    <g id="Group_802" data-name="Group 802" transform="translate(11.514 21.493)">
                      <g id="Group_801" data-name="Group 801">
                        <path id="Path_207" data-name="Path 207" d="M550.9,270.848c-2.391,2.679-4.859,5.308-7.123,8.106-.305.373.2.907.534.534,2.4-2.688,4.6-5.554,6.877-8.352.161-.2-.11-.492-.288-.288Z" transform="translate(-543.679 -270.778)" fill="#fafafa" />
                      </g>
                    </g>
                    <g id="Group_804" data-name="Group 804" transform="translate(33.475 26.091)">
                      <g id="Group_803" data-name="Group 803">
                        <path id="Path_208" data-name="Path 208" d="M569.588,276.573c1.018,4.3,1.628,8.674,2.569,12.982.085.407.789.331.738-.1a97.87,97.87,0,0,0-2.73-13.041.3.3,0,0,0-.577.161Z" transform="translate(-569.578 -276.2)" fill="#fafafa" />
                      </g>
                    </g>
                    <g id="Group_806" data-name="Group 806" transform="translate(48.571 35.703)" opacity="0.3">
                      <g id="Group_805" data-name="Group 805">
                        <path id="Path_209" data-name="Path 209" d="M606.976,287.539a6.306,6.306,0,0,0-4.859,2.425,15.034,15.034,0,0,0-2.518,4.986,51.712,51.712,0,0,0-2.7,17.637c.042,1.832.068,3.934-1.323,5.13-1.187,1.026-2.951.95-4.409,1.518-2.332.916-4.333,4.028-3.655,6.444l13.067,4.952Z" transform="translate(-587.382 -287.536)" />
                      </g>
                    </g>
                  </g>
                </g>
                <g id="Group_810" data-name="Group 810" transform="translate(71.987 106.38)">
                  <g id="Group_809" data-name="Group 809">
                    <path id="Path_210" data-name="Path 210" d="M591.255,320.107a4.755,4.755,0,0,1-1.492.577,17.558,17.558,0,0,1-3.807.6,16.872,16.872,0,0,1-3.841-.263,4.875,4.875,0,0,1-1.535-.441,52.494,52.494,0,0,0,5.359.263C588.907,320.718,591.222,319.971,591.255,320.107Z" transform="translate(-580.58 -320.091)" fill="#263238" />
                  </g>
                </g>
                <g id="Group_818" data-name="Group 818" transform="translate(25.215 67.222)">
                  <g id="Group_811" data-name="Group 811" transform="translate(0 74.94)">
                    <path id="Path_211" data-name="Path 211" d="M535.816,362.29l-10.4,87.227h4.494l11.04-87.227Z" transform="translate(-525.42 -362.29)" fill="#455a64" />
                  </g>
                  <g id="Group_812" data-name="Group 812" transform="translate(51.834 74.94)">
                    <path id="Path_212" data-name="Path 212" d="M586.55,362.29l9.548,87.227H601l-9.31-87.227Z" transform="translate(-586.55 -362.29)" fill="#455a64" />
                  </g>
                  <g id="Group_813" data-name="Group 813" transform="translate(100.98 74.94)">
                    <path id="Path_213" data-name="Path 213" d="M646.757,362.29l7.962,87.227h5.461L651.1,362.29H644.51" transform="translate(-644.51 -362.29)" fill="#455a64" />
                  </g>
                  <g id="Group_814" data-name="Group 814" transform="translate(56.973)">
                    <path id="Path_214" data-name="Path 214" d="M612.308,273.91h38.539a6,6,0,0,1,5.936,6.851l-9.124,63.671a5.159,5.159,0,0,1-5.1,4.426H592.61Z" transform="translate(-592.61 -273.91)" fill="#455a64" />
                  </g>
                  <g id="Group_815" data-name="Group 815" transform="translate(5.266 0.008)">
                    <path id="Path_215" data-name="Path 215" d="M531.63,348.852c.093-.967,0-5.783,0-5.783l47.79.9a7.334,7.334,0,0,0,7.377-6.147l9.531-58.194a6.81,6.81,0,0,1,6.716-5.707h0l-10.4,65.52a11.165,11.165,0,0,1-11.032,9.421H531.63Z" transform="translate(-531.63 -273.92)" fill="#455a64" />
                  </g>
                  <g id="Group_817" data-name="Group 817" transform="translate(58.939)">
                    <g id="Group_816" data-name="Group 816">
                      <path id="Path_216" data-name="Path 216" d="M612.66,273.91a.532.532,0,0,1-.008.2c-.017.161-.042.356-.068.585-.076.551-.186,1.306-.314,2.256-.3,1.993-.721,4.816-1.246,8.284-1.1,7.021-2.612,16.662-4.273,27.3-.856,5.325-1.662,10.4-2.408,15.025-.373,2.306-.729,4.5-1.06,6.555a44.379,44.379,0,0,1-1.162,5.681,15.7,15.7,0,0,1-1.967,4.375,10.969,10.969,0,0,1-2.467,2.654,8.184,8.184,0,0,1-1.993,1.119,4.288,4.288,0,0,1-.763.229,9.933,9.933,0,0,0,2.654-1.484,13.07,13.07,0,0,0,4.24-6.978,46.845,46.845,0,0,0,1.1-5.656c.322-2.06.661-4.257,1.026-6.563.729-4.621,1.535-9.7,2.374-15.034,1.713-10.642,3.273-20.266,4.4-27.286.577-3.46,1.051-6.275,1.382-8.259.17-.941.305-1.7.4-2.247.042-.237.085-.424.119-.585A.6.6,0,0,1,612.66,273.91Z" transform="translate(-594.929 -273.91)" fill="#263238" />
                    </g>
                  </g>
                </g>
                <g id="Group_820" data-name="Group 820" transform="translate(99.74 60.835)">
                  <g id="Group_819" data-name="Group 819">
                    <path id="Path_217" data-name="Path 217" d="M617.592,273.188a18.352,18.352,0,0,1-2.323-3.29,19,19,0,0,1-1.95-3.519,18.361,18.361,0,0,1,2.323,3.29A17.873,17.873,0,0,1,617.592,273.188Z" transform="translate(-613.311 -266.377)" fill="#263238" />
                  </g>
                </g>
                <g id="Group_822" data-name="Group 822" transform="translate(31.289 72.69)">
                  <g id="Group_821" data-name="Group 821">
                    <path id="Path_218" data-name="Path 218" d="M532.589,289.416a34.5,34.5,0,0,1,2.993-4.655,35.254,35.254,0,0,1,3.349-4.4,34.5,34.5,0,0,1-2.993,4.655A36.261,36.261,0,0,1,532.589,289.416Z" transform="translate(-532.584 -280.358)" fill="#263238" />
                  </g>
                </g>
                <g id="Group_824" data-name="Group 824" transform="translate(48.141 66.323)">
                  <g id="Group_823" data-name="Group 823">
                    <path id="Path_219" data-name="Path 219" d="M567.85,272.85a6.267,6.267,0,0,1-.526.975c-.4.7-.907,1.56-1.518,2.6-1.3,2.188-3.129,5.189-5.24,8.454s-4.1,6.164-5.6,8.225c-.738,1.034-1.357,1.857-1.8,2.425a4.872,4.872,0,0,1-.712.848,5.214,5.214,0,0,1,.568-.95c.449-.661,1.009-1.5,1.679-2.5,1.425-2.111,3.366-5.028,5.478-8.293,2.06-3.231,3.926-6.156,5.35-8.378.678-1.077,1.238-1.942,1.636-2.535A5.04,5.04,0,0,1,567.85,272.85Z" transform="translate(-552.457 -272.849)" fill="#263238" />
                  </g>
                </g>
              </g>
            </g>
          </g>
          <g id="webdemo" transform="translate(227.279)">
            <g id="web" transform="translate(0)">
              <g transform="matrix(1, 0, 0, 1, -228.81, -7.5)" filter="url(#Rectangle_6)">
                <rect id="Rectangle_6-2" data-name="Rectangle 6" width="137.365" height="77.162" rx="5" transform="translate(228.81 7.5)" fill="#fff" />
              </g>
              <g id="Group_7" data-name="Group 7" transform="translate(0.424 6.783)">
                <line id="Line_1" data-name="Line 1" x1="136.904" transform="translate(0 10.726)" fill="none" stroke="#263238" strokeWidth="1" />
                <g id="Group_6" data-name="Group 6" transform="translate(5.512 0)">
                  <g id="Ellipse_3" data-name="Ellipse 3" fill="#525bf4" stroke="#263238" strokeWidth="1">
                    <circle cx="2.968" cy="2.968" r="2.968" stroke="none" />
                    <circle cx="2.968" cy="2.968" r="2.468" fill="none" />
                  </g>
                  <g id="Ellipse_4" data-name="Ellipse 4" transform="translate(8.479)" fill="#00ff17" stroke="#263238" strokeWidth="1">
                    <circle cx="2.968" cy="2.968" r="2.968" stroke="none" />
                    <circle cx="2.968" cy="2.968" r="2.468" fill="none" />
                  </g>
                  <g id="Ellipse_5" data-name="Ellipse 5" transform="translate(16.959)" fill="#fff700" stroke="#263238" strokeWidth="1">
                    <circle cx="2.968" cy="2.968" r="2.968" stroke="none" />
                    <circle cx="2.968" cy="2.968" r="2.468" fill="none" />
                  </g>
                </g>
              </g>
              <g id="Group_8" data-name="Group 8" transform="translate(7.631 22.894)">
                <g id="web-4" className="web-lines" transform="translate(0 3.392)" fill="rgba(255,255,255,0)" stroke="#263238" strokeWidth="1">
                  <rect width="68.683" height="8.479" rx="4" stroke="none" />
                  <rect x="0.5" y="0.5" width="67.683" height="7.479" rx="3.5" fill="none" />
                </g>
                <g id="web-3" className="web-lines" transform="translate(0 16.959)" fill="rgba(255,255,255,0)" stroke="#263238" strokeWidth="1">
                  <rect width="68.683" height="8.479" rx="4" stroke="none" />
                  <rect x="0.5" y="0.5" width="67.683" height="7.479" rx="3.5" fill="none" />
                </g>
                <g id="web-1" className="web-lines" transform="translate(81.402 0)" fill="rgba(255,255,255,0)" stroke="#e8505b" strokeWidth="1">
                  <rect width="35.613" height="43.245" rx="8" stroke="none" />
                  <rect x="0.5" y="0.5" width="34.613" height="42.245" rx="7.5" fill="none" />
                </g>
                <g id="web-4-2" className="web-lines" data-name="web-4" transform="translate(0 30.526)" fill="rgba(255,255,255,0)" stroke="#e8505b" strokeWidth="1">
                  <rect width="23.742" height="6.783" rx="3.392" stroke="none" />
                  <rect x="0.5" y="0.5" width="22.742" height="5.783" rx="2.892" fill="none" />
                </g>
                <g id="web-5" className="web-lines" transform="translate(27.982 30.526)" fill="rgba(255,255,255,0)" stroke="#e8505b" strokeWidth="1">
                  <rect width="24.59" height="6.783" rx="3.392" stroke="none" />
                  <rect x="0.5" y="0.5" width="23.59" height="5.783" rx="2.892" fill="none" />
                </g>
              </g>
            </g>
          </g>
          <g id="messages_container" data-name="messages container" transform="translate(5.968 35.757)">
            <g id="message1" transform="translate(295.576 48.009)">
              <g id="Group_839" data-name="Group 839" transform="translate(0 0)">
                <g transform="matrix(1, 0, 0, 1, -303.08, -91.27)" filter="url(#Path_225)">
                  <path id="Path_225-2" data-name="Path 225" d="M13.213,0H137.788C145.085,0,151,7.593,151,16.96v9.691c0,9.367-5.915,16.96-13.213,16.96H0V16.96C0,7.593,5.915,0,13.213,0Z" transform="translate(303.08 91.27)" fill="#fff" />
                </g>
              </g>
              <text id="I_want_software_but_don_t_know_how_to_get_it_" data-name="I want software but don&apos;t know how to get it ??" transform="translate(14.768 6.259)" fill="#263238" fontSize="10" fontFamily="Nunito-SemiBold, Nunito" fontWeight="600" letterSpacing="0.01em"><tspan x="0" y="10">I want software but don&apos;t </tspan><tspan x="0" y="28">know how to get it ??</tspan></text>
            </g>
            <g id="message2" transform="translate(0 0)">
              <g transform="matrix(1, 0, 0, 1, -7.5, -43.26)" filter="url(#Path_275)">
                <path id="Path_275-2" data-name="Path 275" d="M11.871,0h122.95a11.871,11.871,0,0,1,11.871,11.871V42.4H11.871A11.871,11.871,0,0,1,0,30.526V11.871A11.871,11.871,0,0,1,11.871,0Z" transform="translate(7.5 43.26)" fill="#fff" />
              </g>
              <text id="We_are_here_to_help_you._You_are_on_right_place" data-name="We are here to help you. You are on right place" transform="translate(10.175 5.936)" fill="#263238" fontSize="10" fontFamily="Nunito-SemiBold, Nunito" fontWeight="600" letterSpacing="0.01em"><tspan x="0" y="10">We are here to help you. </tspan><tspan x="0" y="24">You are on right place</tspan></text>
            </g>
            <g id="message3" transform="translate(50.028 50.028)">
              <g id="background" transform="translate(0 0)">
                <g transform="matrix(1, 0, 0, 1, -57.53, -93.29)" filter="url(#Rectangle_109)">
                  <path id="Rectangle_109-2" data-name="Rectangle 109" d="M13.143,0H65.715A13.143,13.143,0,0,1,78.858,13.143V26.286a0,0,0,0,1,0,0H13.143A13.143,13.143,0,0,1,0,13.143v0A13.143,13.143,0,0,1,13.143,0Z" transform="translate(57.53 93.29)" fill="#fff" />
                </g>
              </g>
              <g id="dots" transform="translate(13.567 8.479)">
                <circle id="Ellipse_17" className="dot" data-name="Ellipse 17" cx="5.088" cy="5.088" r="5.088" fill="#263238" />
                <circle id="Ellipse_18" className="dot" data-name="Ellipse 18" cx="4.24" cy="4.24" r="4.24" transform="translate(13.567 0.848)" fill="#263238" />
                <circle id="Ellipse_19" className="dot" data-name="Ellipse 19" cx="3.392" cy="3.392" r="3.392" transform="translate(25.438 1.696)" fill="#263238" />
                <circle id="Ellipse_20" className="dot" data-name="Ellipse 20" cx="2.544" cy="2.544" r="2.544" transform="translate(35.613 2.544)" fill="#263238" />
                <circle id="Ellipse_21" className="dot" data-name="Ellipse 21" cx="1.696" cy="1.696" r="1.696" transform="translate(44.093 3.392)" fill="#263238" />
              </g>
            </g>
          </g>
        </g>
      </g>
    </svg>

  )
}
export default HeroImage;