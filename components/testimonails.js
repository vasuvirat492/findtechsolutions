import { useEffect } from 'react';
import { gsap } from "gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import styles from './styles/testimonials.module.css';
gsap.registerPlugin(ScrollTrigger);
const Testimonails = () => {
    useEffect(() => {
        const t1 = gsap.timeline({
            scrollTrigger: {
                trigger: '.testimonial',
                start: 'top bottom',
                stagger: 0.3,
            }
        })
        t1.from('.testimonial', {
            duration: 0.5,
            opacity: 0,
            x: 100,
            stagger: 0.5,
        })
    }, [])
    return (
        <div className={styles.testimonialContainer}>
            <div className="container">
                <h2>Testimonials</h2>
                <div className={styles.testimonails}>
                    <div className={styles.testimonail + " testimonial"}>
                        <div className={styles.authorImage}>

                        </div>
                        <img src="./icons/quote.svg" alt="quote" />
                        <p className={styles.comment}>I found greate help and product for my business.I can always recommend this for any technical help</p>
                        <div className={styles.ratingContainer}>
                            <img src="./icons/rating.svg" alt="quote" />
                            <p className={styles.author}>- Daya,HDDev</p>
                        </div>
                        <div className={styles.closequoteContainer}>
                            <img src="./icons/quote.svg" alt="quote" style={{ 'transform': 'rotate(180deg)' }} className={styles.closedquote} />
                        </div>
                    </div>
                    <div className={styles.testimonail + " testimonial"}>
                        <div className={styles.authorImage}>

                        </div>
                        <img src="./icons/quote.svg" alt="quote" />
                        <p className={styles.comment}>I found greate help and product for my business.I can always recommend this for any technical help</p>
                        <div className={styles.ratingContainer}>
                            <img src="./icons/rating.svg" alt="quote" />
                            <p className={styles.author}>- Daya,HDDev</p>
                        </div>
                        <div className={styles.closequoteContainer}>
                            <img src="./icons/quote.svg" alt="quote" style={{ 'transform': 'rotate(180deg)' }} className={styles.closedquote} />
                        </div>
                    </div>
                    <div className={styles.testimonail + " testimonial"}>
                        <div className={styles.authorImage}>

                        </div>
                        <img src="./icons/quote.svg" alt="quote" />
                        <p className={styles.comment}>I found greate help and product for my business.I can always recommend this for any technical help</p>
                        <div className={styles.ratingContainer}>
                            <img src="./icons/rating.svg" alt="quote" />
                            <p className={styles.author}>- Daya,HDDev</p>
                        </div>
                        <div className={styles.closequoteContainer}>
                            <img src="./icons/quote.svg" alt="quote" style={{ 'transform': 'rotate(180deg)' }} className={styles.closedquote} />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Testimonails;