import { useEffect } from 'react';
import styles from './styles/services.module.css';
import TickIcon from './icons/Tick';
import { gsap } from "gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
gsap.registerPlugin(ScrollTrigger);
const Services = () => {
    useEffect(() => {
        const t1 = gsap.timeline({
            scrollTrigger: {
                trigger: '.featureContainer',
            }
        })
        t1.from('.feature',
            {
                y: 30,
                opacity: 0,
                duration: 1.2,
                delay: 0.5,
                stagger: 0.2,
            })
        gsap.from('.imageContainer', {
            scrollTrigger: {
                trigger: '.imageContainer',
            },
            x: '100%',
            opacity: 0,
            duration: 1,
        })
        //     const t2 = gsap.timeline({
        //         scrollTrigger: {
        //             trigger: '.imageContainer',
        //         }
        //     })
        //     t2.from('.imageContainer', {
        //         x: '100%',
        //         opacity: 0,
        //         // scale: 0,
        //         duration: 1
        //     })

    }, [])
    return (
        <div className={styles.services}>
            <div className="container">
                <h2>Our Services</h2>
                <div className={styles.featuresContainer}>
                    <div className={styles.features}>
                        <div className={styles.featurePart + " featureContainer"}>
                            <p className={styles.featureHeading}>All our software's are having</p>
                            <div className={styles.feature + " feature"}>
                                <TickIcon />
                                <p>Fast loading</p>
                            </div>
                            <div className={styles.feature + " feature"}>
                                <TickIcon />
                                <p>Great SEO</p>
                            </div>
                            <div className={styles.feature + " feature"}>
                                <TickIcon />
                                <p>Security</p>
                            </div>
                            <div className={styles.feature + " feature"}>
                                <TickIcon />
                                <p>Low Pricing</p>
                            </div>
                            <div className={styles.feature + " feature"}>
                                <TickIcon />
                                <p>Maintainance</p>
                            </div>
                            <div className={styles.feature + " feature"}>
                                <TickIcon />
                                <p>24x7 customer support</p>
                            </div>
                        </div>
                    </div>
                    <div className={styles.imageContainer + " imageContainer"}>
                        <img src="./images/services1.jpg" alt="services" />
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Services;